import { CONFIG } from './src/config'

export default {
    entry: 'src/app.js',
    indent: '    ',
    // sourceMap: true,
    external: [
        'joint',
        // 'jquery',
    ],
    paths: {
        joint: './html/js/joint.js',
    },
    // globals: {
    //     $ : 'jquery'
    // },
    targets: [
        {
            format: 'umd',
            moduleName: CONFIG.APP.MODULE_NAME, //'BPScript',
            dest: '../html/js/bpscript.js'
        },
    ]
}
