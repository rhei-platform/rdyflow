/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

var CONFIG = {

    LIBRARY: {
        COMPONENT_TYPE_NAME: 'bpscript.Component',
        NATIVE_PRODUCER_TYPE_NAME: 'bpscript.NativeProducer',
        NATIVE_CONSUMER_TYPE_NAME: 'bpscript.NativeConsumer',
    },

    APP: {
        MODULE_NAME: 'BPScript',
    },

    PAPER: {
        HEADER_HEIGHT: 30,
        BACKGROUND_IMAGE: 'img/graph_bg.png',
    }
}

export { CONFIG }
