/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

import { getGraph } from '../ui/Model'
import { getPaper, _addElementToolsView, _addLinkToolsView } from '../ui/lib'

import { modalNetworkExport } from '../ui/dom/ModalDialogs'

let _attachToolViews = function() {
    let graph = getGraph()
    let nodes = graph.getElements()
    let connections = graph.getLinks()
    for( var idx in nodes ) {
        let node = nodes[idx]
        let nodeView = node.findView( getPaper() )
        _addElementToolsView( nodeView )
    }
    for( var idx in connections ) {
        let connection = connections[idx]
        let connectionView = connection.findView( getPaper() )
        _addLinkToolsView( connectionView )
    }
}

let _generateNetSource = function() {
    let graph = getGraph()
    let nodes = graph.getElements()
    let connections = graph.getLinks()

    let output = []
    let buffers_source = []
    let contexts_source = []
    let connections_source = []

    let nativeFunctionsUsed = new Set()

    for( var idx in nodes ) {
        let node = nodes[idx]
        // console.log( node )
        // console.log( node.getPorts() )
        let bpcomponent = node.get('bpcomponent')
        // console.log( 'type', bpcomponent.type )
        switch( bpcomponent.type ) {
            case 'component':
                contexts_source.push([
                    '[', bpcomponent.name, ']',
                    '<--',
                    bpcomponent.source.replace('/','.'),
                    ''
                ].join(' '))
                break;
            case 'nativeConsumer':
            case 'nativeProducer':
                let bufferName = 'buffer_' + idx
                bpcomponent.bufferName = bufferName

                buffers_source.push([
                    '{ ', bufferName, ' }', '<--\n',
                    bpcomponent.configBuffer, '\n',
                    'EOB <--',
                    ''
                ].join(''))

                if( ! nativeFunctionsUsed.has( bpcomponent.nativeFunction ) ) {

                    nativeFunctionsUsed.add( bpcomponent.nativeFunction )

                    contexts_source.push([
                        '<', bpcomponent.nativeFunction, '>',
                        '<--',
                        bpcomponent.source.replace('/','.'),
                        ''
                    ].join(' '))
                }
                break;
        }
    }

    for( var idx in connections ) {
        let conn = connections[idx]
        let linkSource = conn.get('source')
        let linkTarget = conn.get('target')
        let sourceElement = conn.getSourceElement()
        let targetElement = conn.getTargetElement()
        let sourcePort = sourceElement.getPort( linkSource.port )
        let targetPort = targetElement.getPort( linkTarget.port )
        let sourceBPComponent = sourceElement.get('bpcomponent')
        let targetBPComponent = targetElement.get('bpcomponent')

        if( sourceBPComponent.type == 'nativeProducer' ) {

            if( targetBPComponent.type == 'nativeConsumer' ) {

                connections_source.push([
                    '<', sourceBPComponent.nativeFunction, ',', sourceBPComponent.bufferName, '>',
                    '-->', 
                    '<', targetBPComponent.nativeFunction, ',', targetBPComponent.bufferName, '>',
                    ''
                ].join(' '))

            } else {

                connections_source.push([
                    '<', sourceBPComponent.nativeFunction, ',', sourceBPComponent.bufferName, '>',
                    '-->', 
                    targetPort.bpport.index, ':', '[', targetBPComponent.name, ']',
                    ''
                ].join(' '))
            }

        } else if( targetBPComponent.type == 'nativeConsumer' ) {

            connections_source.push([
                '[', sourceBPComponent.name, ']', ':', sourcePort.bpport.index, 
                '-->', 
                '<', targetBPComponent.nativeFunction, ',', targetBPComponent.bufferName, '>',
                ''
            ].join(' '))

        } else {

            connections_source.push([
                // sourceBPComponent.source, ':', 
                '[', sourceBPComponent.name, ']', ':', 
                // sourcePort.bpport.name, 
                sourcePort.bpport.index, 
                '-->', 
                // targetPort.bpport.name,
                targetPort.bpport.index,
                ':',
                // targetBPComponent.source,
                '[', targetBPComponent.name, ']',
                ''
            ].join(' '))
        }
        // console.log( linkSource )
        // console.log( sourcePort.bpport )
        // console.log( linkTarget )
        // console.log( targetPort.bpport )
    }

    output.push( '; buffers' )
    output.push( buffers_source.join('\n') )
    output.push( '; contexts' )
    output.push( contexts_source.join('\n') )
    output.push( '; connections' )
    output.push( connections_source.join('\n') )

    let outputString = output.join('\n')
    // console.log( outputString )

    return outputString
}

let exportNet = function() {
    // TODO move these 2 lines to somewhere more appropriate
    let bpnetSource = _generateNetSource()
    modalNetworkExport.show( bpnetSource )

    return getGraph().toJSON()
}

let importNet = function( data ) {
    getGraph().fromJSON( data )
    _attachToolViews()
}

export { exportNet, importNet }
