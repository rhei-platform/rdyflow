/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

import { getJSON } from '../util/lib'


let _loadDatabaseComponents = function( databaseRootUrl, dbData ) {

    // In case dbData is empty
    // we want to return a valid promise which is already resolved
    var cpd = $.Deferred()
    cpd.resolve()
    var currentPromise = cpd.promise()

    var previousPromise = null

    for( var comp in dbData ) {

        let components_db_entry = dbData[ comp ]

        currentPromise =
        getJSON( databaseRootUrl + '/' + components_db_entry.source + '/components.json' )
        .done( function( componentsData, textStatus, jqXHR ) {

            for( var comp in componentsData ) {

                let component = componentsData[comp]
                component.name = component.source
                component.source = components_db_entry.source + '/' + component.source
            }

            components_db_entry.components = componentsData
        })
        .then( previousPromise )
        previousPromise = currentPromise
    }

    return currentPromise
}

let loadDatabase = function( databaseRootUrl ) {

    let dfd = $.Deferred()

    getJSON( databaseRootUrl + '/db.json' )
    .done( function( data, textStatus, jqXHR ) {
        _loadDatabaseComponents( databaseRootUrl, data )
        .always(function() {
            dfd.resolve( data )
        })
    })

    return dfd.promise()
}

export { loadDatabase }
