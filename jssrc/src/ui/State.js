/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

var _selectedComponent = null
var _selectedAction = null

let getSelectedComponent = function() {
    return _selectedComponent
}

let setSelectedComponent = function( component ) {
    _selectedComponent = component
}

let getSelectedAction = function() {
    return _selectedAction
}

let setSelectedAction = function( value ) {
    _selectedAction = value
}

export { getSelectedComponent, setSelectedComponent, getSelectedAction, setSelectedAction }
