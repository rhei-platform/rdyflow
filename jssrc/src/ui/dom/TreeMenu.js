/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

let initComponentTreeToggle = function(){

    $('.collapse')
    .on('show.bs.collapse', function (el) {
        let $div = $(el.target)
        let div_id = $div.attr('id')
        // console.log(div_id)

        let $btn = $('[data-target="#'+div_id+'"]')
        $btn.toggleClass('active')
        let $icon = $btn.find('i')
        $icon.toggleClass('icon-chevron-right')
        $icon.toggleClass('icon-chevron-down')
    })
    .on('hide.bs.collapse', function (el) {
        let $div = $(el.target)
        let div_id = $div.attr('id')
        // console.log(div_id)

        let $btn = $('[data-target="#'+div_id+'"]')
        $btn.toggleClass('active')
        let $icon = $btn.find('i')
        $icon.toggleClass('icon-chevron-right')
        $icon.toggleClass('icon-chevron-down')
    })
}

export { initComponentTreeToggle }
