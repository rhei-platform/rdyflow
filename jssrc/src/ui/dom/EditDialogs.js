/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */


let ModalDetails = function( id ) {

    const self = this

    this.template = [
        '<div class="modal fade" id="'+id+'" tabindex="-1" role="dialog" aria-labelledby="'+id+'Label" aria-hidden="true">',
        '  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">',
        '    <div class="modal-content">',
        '      <div class="modal-header">',
        '        <h5 class="modal-title" id="'+id+'Label">Details</h5>',
        '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">',
        '          <span aria-hidden="true">&times;</span>',
        '        </button>',
        '      </div>',
        '      <div class="modal-body">',
        // message,
        '      </div>',
        '      <div class="modal-footer">',
        '        <button type="button" class="btn btn-secondary" data-dismiss="modal">Nah, I changed my mind</button>',
        '        <button type="button" class="btn btn-primary btn_modal_action">Do it</button>',
        '      </div>',
        '    </div>',
        '  </div>',
        '</div>',
    ].join('')
    
    this.show = function( message, actionCallback ) {
        self.actionCallback = actionCallback
        $('#'+id).find('.modal-body').html(message)
        $('#'+id).modal({})
    },

    this.action = function() {
        $('#'+id).modal('hide')
        if( self.actionCallback ) {
            self.actionCallback()
            self.actionCallback = undefined
        }
    }

    $('body').append($(this.template))

    $('#'+id).find('.btn_modal_action').on('click', function(ev) {
        self.action()
    })
}

let ModalComponentNodeDetails = function( id ) {

    const self = this

    this.template = [
        '<div class="modal fade" id="'+id+'" tabindex="-1" role="dialog" aria-labelledby="'+id+'Label" aria-hidden="true">',
        '  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">',
        '    <div class="modal-content">',
        '      <div class="modal-header">',
        '        <h5 class="modal-title" id="'+id+'Label">Native Node Details</h5>',
        '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">',
        '          <span aria-hidden="true">&times;</span>',
        '        </button>',
        '      </div>',
        '      <div class="modal-body">',
        '         <div class="form-group">',
        '           <label for="'+id+'_input">Label</label>',
        '           <input type="text" class="form-control component-node-label" id="'+id+'_input">',
        '         </div>',
        '      </div>',
        '      <div class="modal-footer">',
        '        <button type="button" class="btn btn-secondary" data-dismiss="modal">Nah, I changed my mind</button>',
        '        <button type="button" class="btn btn-primary btn_modal_action">Do it</button>',
        '      </div>',
        '    </div>',
        '  </div>',
        '</div>',
    ].join('')
    
    this.show = function( data, actionCallback ) {
        self.actionCallback = actionCallback

        let message = 'type -> ' + data.type + '<br>'
        message += 'data ' + JSON.stringify(data)

        $('#'+id).find('.component-node-label').val(data.label || '[no label set]')
        $('#'+id).modal({})
    },

    this.action = function() {
        let data = {
            label: $('#'+id).find('.component-node-label').val()
        }
        $('#'+id).modal('hide')
        if( self.actionCallback ) {
            self.actionCallback(data)
            self.actionCallback = undefined
        }
    }

    $('body').append($(this.template))

    $('#'+id).find('.btn_modal_action').on('click', function(ev) {
        self.action()
    })
}

let ModalNativeNodeDetails = function( id ) {

    const self = this

    this.template = [
        '<div class="modal fade" id="'+id+'" tabindex="-1" role="dialog" aria-labelledby="'+id+'Label" aria-hidden="true">',
        '  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">',
        '    <div class="modal-content">',
        '      <div class="modal-header">',
        '        <h5 class="modal-title" id="'+id+'Label">Component Node Options</h5>',
        '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">',
        '          <span aria-hidden="true">&times;</span>',
        '        </button>',
        '      </div>',
        '      <div class="modal-body">',
        '         <div class="form-group">',
        '           <label for="'+id+'_textarea">Config Buffer</label>',
        '           <textarea class="form-control config-buffer-value" id="'+id+'_textarea" rows="3"></textarea>',
        '         </div>',
        '      </div>',
        '      <div class="modal-footer">',
        '        <button type="button" class="btn btn-secondary" data-dismiss="modal">Leave it as is</button>',
        '        <button type="button" class="btn btn-primary btn_modal_action">Change it</button>',
        '      </div>',
        '    </div>',
        '  </div>',
        '</div>',
    ].join('')
    
    this.show = function( data, actionCallback ) {
        self.actionCallback = actionCallback
        $('#'+id).find('.config-buffer-value').val(data.configBuffer || '')
        $('#'+id).modal({})
    },

    this.action = function() {
        let data = {
            configBuffer : $('#'+id).find('.config-buffer-value').val()
        }
        $('#'+id).modal('hide')
        if( self.actionCallback ) {
            self.actionCallback(data)
            self.actionCallback = undefined
        }
    }

    $('body').append($(this.template))

    $('#'+id).find('.btn_modal_action').on('click', function(ev) {
        self.action()
    })
}


let editModalComponentNodeDetails = undefined
let editModalNativeNodeDetails = undefined

let initEdits = function() {

    editModalComponentNodeDetails = new ModalComponentNodeDetails('editModalComponentNodeDetails')
    editModalNativeNodeDetails = new ModalNativeNodeDetails('editModalNativeNodeDetails')
}

export { initEdits, editModalNativeNodeDetails, editModalComponentNodeDetails }
