/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */


const ModalConfirmation = function( id, message ) {

    const self = this

    this.template = [
        '<div class="modal fade" id="'+id+'" tabindex="-1" role="dialog" aria-labelledby="'+id+'Label" aria-hidden="true">',
        '  <div class="modal-dialog modal-sm modal-dialog-centered" role="document">',
        '    <div class="modal-content">',
        '      <div class="modal-header">',
        '        <h5 class="modal-title" id="'+id+'Label">Confirm</h5>',
        '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">',
        '          <span aria-hidden="true">&times;</span>',
        '        </button>',
        '      </div>',
        '      <div class="modal-body">',
        message,
        '      </div>',
        '      <div class="modal-footer">',
        '        <button type="button" class="btn btn-secondary" data-dismiss="modal">Nah, I changed my mind</button>',
        '        <button type="button" class="btn btn-primary btn_modal_action">Do it</button>',
        '      </div>',
        '    </div>',
        '  </div>',
        '</div>',
    ].join('')
    
    this.show = function( actionCallback ) {
        self.actionCallback = actionCallback
        $('#'+id).modal({})
    },

    this.action = function() {
        $('#'+id).modal('hide')
        if( self.actionCallback ) {
            self.actionCallback()
            self.actionCallback = undefined
        }
    }

    $('body').append($(this.template))

    $('#'+id).find('.btn_modal_action').on('click', function(ev) {
        self.action()
    })
}

const ModalNetworkExport = function( id ) {

    const self = this

    this.template = [
        '<div class="modal fade" id="'+id+'" tabindex="-1" role="dialog" aria-labelledby="'+id+'Label" aria-hidden="true">',
        '  <div class="modal-dialog modal-dialog-centered" role="document">',
        '    <div class="modal-content">',
        '      <div class="modal-header">',
        '        <h5 class="modal-title" id="'+id+'Label">BPNET export</h5>',
        '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">',
        '          <span aria-hidden="true">&times;</span>',
        '        </button>',
        '      </div>',
        '      <div class="modal-body">',
        '         <div class="form-group">',
        '           <label for="'+id+'_textarea">Source</label>',
        '           <textarea class="form-control bpnet-source-value" id="'+id+'_textarea" rows="10" style="font-family: monospace;"></textarea>',
        '         </div>',
        '      </div>',
        '      <div class="modal-footer">',
        // '        <button type="button" class="btn btn-secondary" data-dismiss="modal">Nah, I changed my mind</button>',
        '        <button type="button" class="btn btn-primary btn_modal_action">Nice</button>',
        '      </div>',
        '    </div>',
        '  </div>',
        '</div>',
    ].join('')
    
    this.show = function( content ) {
        $('#'+id).find('.bpnet-source-value').val(content)
        $('#'+id).modal({})
    },

    this.action = function() {
        $('#'+id).modal('hide')
        $('#'+id).find('.bpnet-source-value').val('')
    }

    $('body').append($(this.template))

    $('#'+id).find('.btn_modal_action').on('click', function(ev) {
        self.action()
    })
}

let modalConfirmDeleteAll = undefined
let modalConfirmDeleteNode = undefined

let modalNetworkExport = undefined

let initModals = function() {

    modalConfirmDeleteAll = new ModalConfirmation('modalConfirmDeleteAll', 'Delete all nodes in this network?')
    modalConfirmDeleteNode = new ModalConfirmation('modalConfirmDeleteNode', 'Delete selected nodes?')

    modalNetworkExport = new ModalNetworkExport('modalNetworkExport', '')
}

export { initModals, modalConfirmDeleteAll, modalConfirmDeleteNode, modalNetworkExport }
