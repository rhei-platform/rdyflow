/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

import { getSelectedComponent, setSelectedComponent } from '../../ui/lib'


let _getIconHtml = function( icon ) {

    var icon_html =
    `
    <i class="icon icon-file"></i>
    `
    if( icon ) {
        icon_html =
        `
        <img class="icon-list-item icon-component" src="${icon}"/>
        `
    }

    return icon_html
}

let _createListCategoryItem = function( $parent, list_id, label, icon ) {

    // an actual <button> tag doesn't work well with draggable attribute in a sense
    // that you have to click on button text in order to start dragging it
    // while with <a> tag you can click anywhere inside item cell
    let button_html = 
    // `
    // <a href="#${list_id}" class="list-group-item list-group-item-action" data-toggle="collapse">
    //     <span class="icon-list-item"></span>
    //     ${label}
    // </a>
    // `
    `
    <button class="list-group-item list-group-item-action" type="button" data-toggle="collapse" data-target="#${list_id}">
        <i class="icon icon-chevron-right mr-2"></i>
        ${_getIconHtml( icon )}
        ${label}
    </button>
    `

    let $button = $(button_html)
    $parent.append($button)

    let div_html = `<div class="list-group collapse" id="${list_id}">empty</div>`
    let $category_div = $(div_html)
    $parent.append($category_div)

    return $category_div
}

let _createListComponentItem = function( $parent, list_id, label, icon ) {

    let button_html = 
    `
    <a href="#${list_id}" class="list-group-item list-group-item-action" data-toggle="collapse">
        ${_getIconHtml( icon )}
        ${label}
    </a>
    `

    let $button = $(button_html)
    $parent.append($button)

    // this is a component item
    // we should be able to drag-drop it onto jointjs paper canvas
    $button.attr('draggable', true)
    // use dragStartHandler.name here so we don't miss *string* reference here if dragStartHandler gets refactored
    // $button.attr('ondragstart', `${CONFIG.APP.MODULE_NAME}.${dragStartHandler.name}(event)`)
    $button.attr('ondragstart', 'BPScript.dragStartHandler(event)')

    return $button
}


//
// populate component list
//

let _setActiveComponent = function(ev) {

    let $button = $(ev.target)
    setSelectedComponent( $button.data('component') )
    console.log('selectedComponent', getSelectedComponent())
}

let _listComponents = function( $parent, data, category_idx ) {

    var component_idx = 1

    for( var comp in data ) {

        // console.log(comp)
        let component = data[comp]
        // console.log(component)
        let $item_button = _createListComponentItem( $parent, `component-${category_idx}-${component_idx}`, component.label, component.icon )
        $item_button.data( 'component', component )
        $item_button.on( 'click', _setActiveComponent )
        component_idx = component_idx + 1
    }
}

let displayComponentDatabase = function( data, parentElementId ) {

    var category_idx = 1
    for( var comp in data ) {

        let components_db_entry = data[ comp ]

        let $parent = $(`#${parentElementId}`)
        let $components_div = _createListCategoryItem( $parent, `category-${category_idx}`, components_db_entry.label, components_db_entry.icon )
        $components_div.html('')

        _listComponents( $components_div, components_db_entry.components, category_idx )

        category_idx = category_idx + 1
    }
}

export { displayComponentDatabase }
