/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

import { getGraph } from '../../ui/Model'
import { getSelectedComponent, setSelectedComponent } from '../../ui/lib'
import { makeNetworkNode } from '../../ui/lib'

import { getPaper, _addElementToolsView } from '../jointjs/Paper'

function dragOverHandler(ev) {
    // console.log('dragOverHandler',ev)
    ev.preventDefault()
}

function dragStartHandler(ev) {
    // console.log('dragStartHandler',ev)
    // console.log('ev.srcElement',ev.srcElement)
    let $button = $(ev.srcElement)
    // console.log('$button',$button)
    // console.log('$button.data(\'component\')',$button.data('component'))
    setSelectedComponent( $button.data('component') )
    // console.log('selectedComponent',getSelectedComponent())

    // var img = new Image()
    var img = document.createElement("img")
    let icon_src = $('#dnd-handler img').attr('src') || 'icons/box.svg'
    img.src = icon_src
    // console.log('img', img)

    ev.dataTransfer.setDragImage(img, 24, 24)
}


function dropHandler(ev) {
    // console.log('dropHandler',ev.layerX, ev.layerY)
    ev.preventDefault()
    // var data = ev.dataTransfer.getData("text")
    // console.log('dropHandler -> data',data)

    if( getSelectedComponent() ) {

        var newNode = makeNetworkNode( getSelectedComponent() )

        newNode.position(ev.layerX, ev.layerY)
        newNode.resize(120, 80)
        newNode.addTo( getGraph() )
        // FIXME: we are referencing a global paper instance here!
        // we should probably create and add this somewhere else or what?
        var componentView = newNode.findView( getPaper() )
        _addElementToolsView( componentView )

        setSelectedComponent( null )
    }
}

export { dragStartHandler, dragOverHandler, dropHandler }
