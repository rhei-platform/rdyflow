/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

import { getSelectedAction } from '../State'
import { getGraph } from '../Model'
import { CONFIG } from '../../config';
import { BtnRemove, BtnInfo } from '../../ui/lib'

// JointJS adds elements and links to graph and
// then adds graph to a paper.
// There is no way to know to which paper a graph is added
// if you only have graph reference.
// We have only one paper instance (for now) in GUI so
// we can get away with using this reference all around
// the code base.
let _paper = undefined

let getPaper = function() {
    return _paper
}

let _makePaper = function( options ) {

    var paperOpts = joint.util.assign({
        gridSize: 1,
        background: {
            image: CONFIG.PAPER.BACKGROUND_IMAGE,
            position: 'top left',
            repeat: 'repeat',
            opacity: 0.8
        },
        //
        // Configure links
        //
        linkPinning: false,
        defaultLink: new joint.shapes.standard.Link({
            connector: {
                name: 'smooth',
            },
            attrs: {
                line: { // selector for the visible <path> SVGElement
                    stroke: '#e7e7e7' // SVG attribute and value
                }
            }
        }),

        validateConnection: function( cellViewS, magnetS, cellViewT, magnetT, end, linkView ) {
            // console.log("magnetS.getAttribute('port-group')", magnetS && magnetS.getAttribute('port-group'))
            // console.log("magnetT.getAttribute('port-group')", magnetT && magnetT.getAttribute('port-group'))

            // Prevent linking from output ports to input ports within one element.
            if( cellViewS === cellViewT ) return false;
            // Only allow links which end on input ports.
            return magnetT && magnetT.getAttribute('port-group') === 'inputs';
        },

        validateMagnet: function( cellView, magnet ) {
            // console.log("magnet.getAttribute('port-group')", magnet && magnet.getAttribute('port-group'))

            // Only allow linking interaction when starting link from output ports
            return magnet && magnet.getAttribute('port-group') === 'outputs';
        },

    }, options)

    var paper = new joint.dia.Paper( paperOpts )

    return paper
}

let _defineActionDragSelection = function( paper ) {

    var _selectedElements = []

    let beginSelection = function( evt, x, y, model ) {

        let rectangle = new joint.shapes.standard.Rectangle({
            position: {
                x: x,
                y: y,
            },
            size: {
                width: 2,
                height: 2,
            },
            attrs: {
                body: {
                    fill: 'none',
                    strokeDasharray: '5,5',
                }
            },
        })

        rectangle.addTo( model )
        rectangle.toBack()

        evt.data = { rectangle: rectangle, x: x, y: y }
    }

    let doSelection = function( evt, x, y ) {

        let dx = x - evt.data.x
        let dy = y - evt.data.y
        evt.data.rectangle.resize( dx, dy )

        let currentlySelectedElements = []
        let elements = getGraph().findModelsInArea( evt.data.rectangle.getBBox() )

        for( var idx in elements ) {

            let element = elements[ idx ]

            if( element == evt.data.rectangle ) continue
            currentlySelectedElements.push( element )

            let elementView = paper.findViewByModel( element )
            elementView.highlight()
        }

        let unselectedElements = joint.util.difference( _selectedElements, currentlySelectedElements )
        for( var idx in unselectedElements ) {

            let element = unselectedElements[ idx ]

            let elementView = paper.findViewByModel( element )
            elementView.unhighlight()
        }

        _selectedElements = currentlySelectedElements
    }

    let endSelection = function( evt ) {

        let elements = getGraph().findModelsInArea( evt.data.rectangle.getBBox() )

        for( var idx in elements ) {
            let elementView = paper.findViewByModel( elements[ idx ] )
            elementView.unhighlight()
        }

        evt.data.rectangle.remove()
    }

    paper.on({

        ////////////////////////////////////////////////////////////////
        //
        // MULTI ELEMENT SELECTION SUPPORT
        //
        'blank:pointerdown': function( evt, x, y ) {
            // console.log('blank:pointerdown', evt.data)

            let action = getSelectedAction()
            // console.log('action', action)

            if( action == 'select' )
                beginSelection( evt, x, y, this.model )

        },

        'blank:pointermove': function( evt, x, y ) {

            let action = getSelectedAction()
            // console.log('action', action)

            if( action == 'select' )
                doSelection( evt, x, y )
        },

        'blank:pointerup': function( evt ) {
            // console.log('blank:pointerup', evt.data)

            let action = getSelectedAction()
            // console.log('action', action)

            if( action == 'select' )
                endSelection( evt )
        },
        //
        ////////////////////////////////////////////////////////////////
    })
}

function _addElementToolsView( elementView ) {

    //////////////////////////////////////////////////////////////////
    // START tools view

    // 1) creating element tools
    var removeTool = new BtnRemove({
        x: 10,
        // y: 0,
    })
    var infoTool = new BtnInfo({
        x: 25,
        // y: 0,
    })

    // 2) creating a tools view
    var toolsView = new joint.dia.ToolsView({
        name: 'basic-element-tools',
        tools: [removeTool, infoTool]
    })

    // 3) attaching to a link view
    elementView.addTools(toolsView)
    elementView.hideTools()

    // END tools view
    //////////////////////////////////////////////////////////////////
}

function _addLinkToolsView( linkView ) {

    // add link tools
    // var verticesTool = new joint.linkTools.Vertices()
    // var segmentsTool = new joint.linkTools.Segments()
    // var boundaryTool = new joint.linkTools.Boundary()
    var removeTool = new joint.linkTools.Remove()

    // 2) creating a tools view
    var toolsView = new joint.dia.ToolsView({
        name: 'basic-link-tools',
        tools: [removeTool]
    })

    // 3) attaching to a link view
    // var linkView = link.findView(paper);
    linkView.addTools( toolsView )
    linkView.hideTools()
}

function _defineActionLinkToolsView( paper ) {

    paper.on({

        ////////////////////////////////////////////////////////////////
        //
        // LINK TOOLS
        //
        'link:connect' : function( linkView, evt, elementViewConnected, magnet, arrowhead ) {
            // console.log('link:connect', linkView, evt)
            _addLinkToolsView( linkView )
        },
        'link:mouseenter': function( linkView ) {
            linkView.showTools()
        },
        'link:mouseleave': function( linkView ) {
            linkView.hideTools()
        },
        //
        ////////////////////////////////////////////////////////////////
    })
}

function _defineActionElementToolsView( paper ) {

    paper.on({

        ////////////////////////////////////////////////////////////////
        //
        // ELEMENT TOOLS
        //
        'element:mouseenter': function( elementView, evt ) {
            // console.log('element:mouseenter', elementView, evt)
            elementView.showTools()
        },

        'element:mouseleave': function( elementView, evt ) {
            // console.log('element:mouseleave', elementView, evt)
            elementView.hideTools()
        },
        //
        ////////////////////////////////////////////////////////////////
    })
}

let initPaper = function( canvasId ) {

    // TODO move references to DOM elements/selectors into their own module
    // and then pull them in via methods like getGraph() etc.
    _paper = _makePaper({
        el: document.getElementById( canvasId ),
        model: getGraph(),
        // width: '100%',
        // height: '100%', // bgnd is 128x128
    })

    getGraph().on('add', function(cell) { 
        // Only handle elements
        if( ! cell.isElement() ) return

        // Only handle bpscript.Component types
        if( cell.attributes.type != CONFIG.LIBRARY.COMPONENT_TYPE_NAME &&
            cell.attributes.type != CONFIG.LIBRARY.NATIVE_PRODUCER_TYPE_NAME &&
            cell.attributes.type != CONFIG.LIBRARY.NATIVE_CONSUMER_TYPE_NAME ) return

        // console.log( 'cell', cell )
        cell.adjustViewSize( _paper )
    })

    _paper.on('render:done', function(evt, x, y) {
        console.log('render:done.')
    })

    _defineActionDragSelection( _paper )
    _defineActionElementToolsView( _paper )
    _defineActionLinkToolsView( _paper )
}

let clearPaper = function() {
    getGraph().clear()
}

export { initPaper, clearPaper, getPaper, _addElementToolsView, _addLinkToolsView }
