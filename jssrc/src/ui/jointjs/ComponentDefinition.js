/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

import { CONFIG } from '../../config'

import { modalConfirmDeleteNode } from '../dom/ModalDialogs'
import { editModalNativeNodeDetails, editModalComponentNodeDetails } from '../dom/EditDialogs'


const BtnRemove = joint.elementTools.Button.extend({
    children: [{
        tagName: 'circle',
        selector: 'button',
        attributes: {
            'r': 7,
            'fill': '#DF691A',
            'cursor': 'pointer'
        }
    }, {
        tagName: 'path',
        selector: 'icon',
        attributes: {
            'd': 'M -3 -3 3 3 M -3 3 3 -3',
            'fill': 'none',
            'stroke': '#FFFFFF',
            'stroke-width': 2,
            'pointer-events': 'none'
        }
    }],
    options: {
        distance: 60,
        offset: 0,
        action: function(evt, view, tool) {
            modalConfirmDeleteNode.show(function(){
                view.model.remove({ ui: true, tool: tool.cid })
            })
        }
    }
})

const BtnInfo = joint.elementTools.Button.extend({
    children: [{
        tagName: 'circle',
        selector: 'button',
        attributes: {
            'r': 7,
            'fill': '#959EA7',
            'cursor': 'pointer'
        }
    }, {
        tagName: 'path',
        selector: 'icon',
        attributes: {
            'd': 'M -2 4 2 4 M 0 3 0 0 M -2 -1 1 -1 M -1 -4 1 -4',
            'fill': 'none',
            'stroke': '#FFFFFF',
            'stroke-width': 2,
            'pointer-events': 'none'
        }
    }],
    options: {
        // x: '100%',
        // y: 0,
        action: function(evt) {
            // let message = 'View id: ' + this.id + '<br>' + 'Model id: ' + this.model.id + '<br>'
            let bpcomponent = this.model.get('bpcomponent')
            console.log('what have we got here',bpcomponent)

            if( bpcomponent.type == 'component' ) {

                editModalComponentNodeDetails.show( bpcomponent, function(data) {
                    bpcomponent.label = data.label
                    console.log('the right stuff',bpcomponent)
                })

            } else if( bpcomponent.type == 'nativeProducer' || bpcomponent.type == 'nativeConsumer' ) {

                editModalNativeNodeDetails.show( bpcomponent, function(data) {
                    let cb = data.configBuffer.split('\n').map(el => ' '+el.trim()).filter( el => el.trim() ).join('\n')
                    bpcomponent.configBuffer = cb
                    console.log('the right stuff',bpcomponent)
                })
            }
        }
    }
})


let inputPortsGroup = {
    position: {
        // name: 'left', // layout name
        // args: {
        //     dy: 10,
        // }, // arguments for port layout function, properties depends on type of layout
        name: 'line',
        args: {
            start: { x: 0, y: CONFIG.PAPER.HEADER_HEIGHT },
            end: { x: 0, y: '100%' },
        },
    },
    label: {
        position: {
            name: 'right',
            args: { y: 0 } // extra arguments for the label layout function, see `layout.PortLabel` section
        },
        markup: [{
            tagName: 'text',
            selector: 'text',
            className: 'label-text',
        }],
    },
    markup: [{
        tagName: 'circle',
        selector: 'circle',
    }],
    attrs: {
        circle: {
            r: 8,
            stroke: '#00a8f4',
            strokeWidth: 2,
            fill: '#292929',
            magnet: true,
        },
        text: {
            fontFamily: 'Arial, Helvetica, sans-serif',
            fontSize: 'smaller',
            fill: '#e7e7e7',
        }
    },
}

let outputPortsGroup = {
    position: {
        // name: 'right', // layout name
        // args: {
        //     dy: 10,
        // }, // arguments for port layout function, properties depends on type of layout
        name: 'line',
        args: {
            start: { x: '100%', y: CONFIG.PAPER.HEADER_HEIGHT },
            end: { x: '100%', y: '100%' },
        },
    },
    label: {
        position: {
            name: 'left',
            args: { y: 0 } // extra arguments for the label layout function, see `layout.PortLabel` section
        },
        markup: [{
            tagName: 'text',
            selector: 'text',
            className: 'label-text',
        }],
    },
    markup: [{
        tagName: 'circle',
        selector: 'circle',
    }],
    attrs: {
        circle: {
            r: 8,
            stroke: '#00a8f4',
            strokeWidth: 2,
            fill: '#292929',
            magnet: true,
        },
        text: {
            fontFamily: 'Arial, Helvetica, sans-serif',
            fontSize: 'smaller',
            fill: '#e7e7e7',
        }
    },
}

const BPScriptComponent = 
joint.dia.Element.define(CONFIG.LIBRARY.COMPONENT_TYPE_NAME, {
    //
    // overrides for default attributes
    //
    attrs: {
        body: {
            refWidth: '100%',
            refHeight: '100%',
            strokeWidth: 2,
            stroke: '#000000',
            fill: '#232323',
            rx: 5,
            ry: 5,
        },
        bodyText: {
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            refY: '50%',
            refY2: 15,
            fontFamily: 'Arial, Helvetica, sans-serif',
            fontSize: 14,
            fill: '#333333'
        },
        header: {
            refWidth: '100%',
            height: CONFIG.PAPER.HEADER_HEIGHT,
            strokeWidth: 2,
            stroke: '#000000',
            fill: '#3e3e3e',
            rx: 5,
            ry: 5,
        },
        headerText: {
            textVerticalAnchor: 'middle',
            textAnchor: 'middle',
            refX: '50%',
            refY: 15,
            fontFamily: 'Arial, Helvetica, sans-serif',
            fontSize: 16,
            fill: '#e7e7e7'
        },
    },
    ports: {
        groups: {
            'inputs': inputPortsGroup,
            'outputs': outputPortsGroup,
        },
        items: []
    },
    bpcomponent: {
    }
    // portMarkup: '<rect width="20" height="20" fill="black"/>',
}, {
    //
    // prototype properties
    //
    markup: [{
        tagName: 'rect',
        selector: 'body',
    }, {
        tagName: 'rect',
        selector: 'header'
    }, {
        tagName: 'text',
        selector: 'headerText'
    }, {
        tagName: 'text',
        selector: 'bodyText'
    }],

    addInPort: function( portIndex, portLabel, portColor = '#00a8f4' ) {
        // console.log('this', this)

        var port = {
            // id: 'abc', // generated if `id` value is not present
            group: 'inputs',
            args: {}, // extra arguments for the port layout function, see `layout.Port` section
            label: {
                // ....
            },
            attrs: {
                text: {
                    text: portLabel
                },
                circle: {
                    stroke: portColor,
                }
            },
            bpport: {
                index: portIndex,
                name: portLabel,
            },
        }

        return this.addPort(port)
    },

    addOutPort: function( portIndex, portLabel, portColor = '#00a8f4' ) {
        // console.log('this', this)

        var port = {
            // id: 'abc', // generated if `id` value is not present
            group: 'outputs',
            args: {}, // extra arguments for the port layout function, see `layout.Port` section
            label: {
                // ....
            },
            attrs: {
                text: {
                    text: portLabel
                },
                circle: {
                    stroke: portColor,
                }
            },
            bpport: {
                index: portIndex,
                name: portLabel,
            },
        };

        return this.addPort(port)
    },

    adjustViewSize: function( paper ) {

        var cellView = this.findView( paper )

        let padding = 20
        var cellWidth = this.attributes.size.width

        let $headerText = cellView.$('[joint-selector="headerText"]')
        let headerTextWidth = $headerText[0].getBoundingClientRect().width + padding

        cellWidth = headerTextWidth > cellWidth ? headerTextWidth : cellWidth

        let $ports = cellView.$('.joint-port')
        var maxPortWidth = 0

        $ports.each(function(idx, $el) {

            let portWidth = $el.getBoundingClientRect().width
            if( portWidth > maxPortWidth ) {
                maxPortWidth = portWidth
            }
        })

        maxPortWidth = maxPortWidth * 2 + padding
        cellWidth = maxPortWidth > headerTextWidth ? maxPortWidth : headerTextWidth

        cellWidth = Math.ceil( cellWidth )
        let cellHeight = this.attributes.size.height

        return this.resize( cellWidth, cellHeight )
    },
})

const BPScriptNativeConsumer = 
BPScriptComponent.define(CONFIG.LIBRARY.NATIVE_CONSUMER_TYPE_NAME, {
    //
    // overrides for default attributes
    //
    attrs: {
        header: {
            fill: '#959595',
        },
        body: {
            fill: '#6b6b6b',
        },
    },
})

const BPScriptNativeProducer = 
BPScriptComponent.define(CONFIG.LIBRARY.NATIVE_PRODUCER_TYPE_NAME, {
    //
    // overrides for default attributes
    //
    attrs: {
        header: {
            fill: '#959595',
        },
        body: {
            fill: '#6b6b6b',
        },
    },
})

export { BPScriptComponent, BPScriptNativeConsumer, BPScriptNativeProducer, BtnRemove, BtnInfo }
