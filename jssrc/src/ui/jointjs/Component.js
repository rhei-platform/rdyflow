/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

import {
    BPScriptComponent,
    BPScriptNativeConsumer,
    BPScriptNativeProducer
} from './ComponentDefinition'

let _makeComponent = function( options ) {

    let headerLabel = options.label || 'component'
    let inputs = options.inputs || []
    let outputs = options.outputs || []

    var component = new BPScriptComponent({
        ports: {
            items: []
        },
        bpcomponent: joint.util.assign( {}, options )
    })

    component.attr({
        headerText: {
            text: headerLabel,
        },
    })

    for( var idx in inputs) {

        let element = inputs[ idx ]
        let label = element.name || 'IN'
        let color = element.pinColor || 'white'

        component.addInPort( idx, label, color )
    }

    for( var idx in outputs) {

        let element = outputs[ idx ]
        let label = element.name || 'OUT'
        let color = element.pinColor || 'white'

        component.addOutPort( idx, label, color )
    }

    return component
}

let _makeConsumer = function( options ) {

    let headerLabel = options.label || 'native consumer'

    let component = new BPScriptNativeConsumer({
        ports: {
            items: []
        },
        bpcomponent: joint.util.assign( {}, options )
    })

    component.attr({
        headerText: {
            text: headerLabel,
        },
    })

    let label = options.port.name || 'IN'
    let color = options.port.pinColor || 'white'
    component.addInPort( 0, label, color )

    return component
}

let _makeProducer = function( options ) {

    let headerLabel = options.label || 'native producer'

    let component = new BPScriptNativeProducer({
        ports: {
            items: []
        },
        bpcomponent: joint.util.assign( {}, options )
    })

    component.attr({
        headerText: {
            text: headerLabel,
        },
    })

    let label = options.port.name || 'OUT'
    let color = options.port.pinColor || 'white'
    component.addOutPort( 0, label, color )

    return component
}

let makeNetworkNode = function( options ) {

    options = options || {}
    let args = joint.util.assign( {}, options )
    // console.log('args', args)

    if( args.type == 'component' ) {
        return _makeComponent( args )
    }

    if( args.type == 'nativeProducer' ) {
        return _makeProducer( args )
    }

    if( args.type == 'nativeConsumer' ) {
        return _makeConsumer( args )
    }
}


export { makeNetworkNode }
