/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

import { initModals } from './dom/ModalDialogs'
import { initEdits } from './dom/EditDialogs'

let initDOM = function() {
    initModals()
    initEdits()
}

export { initDOM }
