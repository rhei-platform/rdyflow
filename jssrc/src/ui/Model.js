/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */

var _graph = new joint.dia.Graph()

let getGraph = function() {
    return _graph
}

export { getGraph }
