/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: GPL-3.0-or-later */


let getJSON = function( url ) {

    return $.get({
        dataType: 'json',
        success: null,
        url: url
    })
    .fail( function( jqXHR, textStatus, errorThrown ) {
        console.error( `GET "${url}" -> ${jqXHR.status} - '${errorThrown}'` )
    })
}

export { getJSON }
