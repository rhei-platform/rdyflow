(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define(['exports'], factory) :
    (factory((global.BPScript = global.BPScript || {})));
}(this, (function (exports) { 'use strict';

    // Polyfills

    if ( Number.EPSILON === undefined ) {

    	Number.EPSILON = Math.pow( 2, - 52 );

    }

    if ( Number.isInteger === undefined ) {

    	// Missing in IE
    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger

    	Number.isInteger = function ( value ) {

    		return typeof value === 'number' && isFinite( value ) && Math.floor( value ) === value;

    	};

    }

    //

    if ( Math.sign === undefined ) {

    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/sign

    	Math.sign = function ( x ) {

    		return ( x < 0 ) ? - 1 : ( x > 0 ) ? 1 : + x;

    	};

    }

    if ( Function.prototype.name === undefined ) {

    	// Missing in IE
    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/name

    	Object.defineProperty( Function.prototype, 'name', {

    		get: function () {

    			return this.toString().match( /^\s*function\s*([^\(\s]*)/ )[ 1 ];

    		}

    	} );

    }

    if ( Object.assign === undefined ) {

    	// Missing in IE
    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign

    	( function () {

    		Object.assign = function ( target ) {

    			'use strict';

    			if ( target === undefined || target === null ) {

    				throw new TypeError( 'Cannot convert undefined or null to object' );

    			}

    			var output = Object( target );

    			for ( var index = 1; index < arguments.length; index ++ ) {

    				var source = arguments[ index ];

    				if ( source !== undefined && source !== null ) {

    					for ( var nextKey in source ) {

    						if ( Object.prototype.hasOwnProperty.call( source, nextKey ) ) {

    							output[ nextKey ] = source[ nextKey ];

    						}

    					}

    				}

    			}

    			return output;

    		};

    	} )();

    }

    if( Array.prototype.includes === undefined ) {

    	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes#Polyfill

    	// https://tc39.github.io/ecma262/#sec-array.prototype.includes
    	Object.defineProperty( Array.prototype, 'includes', {
    		value: function( valueToFind, fromIndex ) {
    	
    			if (this == null) {
    				throw new TypeError('"this" is null or not defined');
    			}
    	
    			// 1. Let O be ? ToObject(this value).
    			var o = Object(this);
    	
    			// 2. Let len be ? ToLength(? Get(O, "length")).
    			var len = o.length >>> 0;
    	
    			// 3. If len is 0, return false.
    			if (len === 0) {
    				return false;
    			}
    	
    			// 4. Let n be ? ToInteger(fromIndex).
    			//    (If fromIndex is undefined, this step produces the value 0.)
    			var n = fromIndex | 0;
    	
    			// 5. If n ≥ 0, then
    			//  a. Let k be n.
    			// 6. Else n < 0,
    			//  a. Let k be len + n.
    			//  b. If k < 0, let k be 0.
    			var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
    	
    			function sameValueZero(x, y) {
    				return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
    			}
    	
    			// 7. Repeat, while k < len
    			while (k < len) {
    				// a. Let elementK be the result of ? Get(O, ! ToString(k)).
    				// b. If SameValueZero(valueToFind, elementK) is true, return true.
    				if (sameValueZero(o[k], valueToFind)) {
    					return true;
    				}
    				// c. Increase k by 1. 
    				k++;
    			}
    	
    			// 8. Return false
    			return false;
    		}
    	});
    }

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach#Polyfill
    // Production steps of ECMA-262, Edition 5, 15.4.4.18
    // Reference: http://es5.github.io/#x15.4.4.18
    if( Array.prototype.forEach === undefined ) {

    	Array.prototype.forEach = function(callback/*, thisArg*/) {

    		var T, k;

    		if (this == null) {
    			throw new TypeError('this is null or not defined');
    		}

    		// 1. Let O be the result of calling toObject() passing the
    		// |this| value as the argument.
    		var O = Object(this);

    		// 2. Let lenValue be the result of calling the Get() internal
    		// method of O with the argument "length".
    		// 3. Let len be toUint32(lenValue).
    		var len = O.length >>> 0;

    		// 4. If isCallable(callback) is false, throw a TypeError exception. 
    		// See: http://es5.github.com/#x9.11
    		if (typeof callback !== 'function') {
    			throw new TypeError(callback + ' is not a function');
    		}

    		// 5. If thisArg was supplied, let T be thisArg; else let
    		// T be undefined.
    		if (arguments.length > 1) {
    			T = arguments[1];
    		}

    		// 6. Let k be 0.
    		k = 0;

    		// 7. Repeat while k < len.
    		while (k < len) {

    			var kValue;

    			// a. Let Pk be ToString(k).
    			//    This is implicit for LHS operands of the in operator.
    			// b. Let kPresent be the result of calling the HasProperty
    			//    internal method of O with argument Pk.
    			//    This step can be combined with c.
    			// c. If kPresent is true, then
    			if (k in O) {

    				// i. Let kValue be the result of calling the Get internal
    				// method of O with argument Pk.
    				kValue = O[k];

    				// ii. Call the Call internal method of callback with T as
    				// the this value and argument list containing kValue, k, and O.
    				callback.call(T, kValue, k, O);
    			}
    			// d. Increase k by 1.
    			k++;
    		}
    		// 8. return undefined.
    	};
    }

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    let _getIconHtml = function( icon ) {

        var icon_html =
        `
    <i class="icon icon-file"></i>
    `;
        if( icon ) {
            icon_html =
            `
        <img class="icon-list-item icon-component" src="${icon}"/>
        `;
        }

        return icon_html
    };

    let _createListCategoryItem = function( $parent, list_id, label, icon ) {

        // an actual <button> tag doesn't work well with draggable attribute in a sense
        // that you have to click on button text in order to start dragging it
        // while with <a> tag you can click anywhere inside item cell
        let button_html = 
        // `
        // <a href="#${list_id}" class="list-group-item list-group-item-action" data-toggle="collapse">
        //     <span class="icon-list-item"></span>
        //     ${label}
        // </a>
        // `
        `
    <button class="list-group-item list-group-item-action" type="button" data-toggle="collapse" data-target="#${list_id}">
        <i class="icon icon-chevron-right mr-2"></i>
        ${_getIconHtml( icon )}
        ${label}
    </button>
    `;

        let $button = $(button_html);
        $parent.append($button);

        let div_html = `<div class="list-group collapse" id="${list_id}">empty</div>`;
        let $category_div = $(div_html);
        $parent.append($category_div);

        return $category_div
    };

    let _createListComponentItem = function( $parent, list_id, label, icon ) {

        let button_html = 
        `
    <a href="#${list_id}" class="list-group-item list-group-item-action" data-toggle="collapse">
        ${_getIconHtml( icon )}
        ${label}
    </a>
    `;

        let $button = $(button_html);
        $parent.append($button);

        // this is a component item
        // we should be able to drag-drop it onto jointjs paper canvas
        $button.attr('draggable', true);
        // use dragStartHandler.name here so we don't miss *string* reference here if dragStartHandler gets refactored
        // $button.attr('ondragstart', `${CONFIG.APP.MODULE_NAME}.${dragStartHandler.name}(event)`)
        $button.attr('ondragstart', 'BPScript.dragStartHandler(event)');

        return $button
    };


    //
    // populate component list
    //

    let _setActiveComponent = function(ev) {

        let $button = $(ev.target);
        setSelectedComponent( $button.data('component') );
        console.log('selectedComponent', getSelectedComponent());
    };

    let _listComponents = function( $parent, data, category_idx ) {

        var component_idx = 1;

        for( var comp in data ) {

            // console.log(comp)
            let component = data[comp];
            // console.log(component)
            let $item_button = _createListComponentItem( $parent, `component-${category_idx}-${component_idx}`, component.label, component.icon );
            $item_button.data( 'component', component );
            $item_button.on( 'click', _setActiveComponent );
            component_idx = component_idx + 1;
        }
    };

    let displayComponentDatabase$$1 = function( data, parentElementId ) {

        var category_idx = 1;
        for( var comp in data ) {

            let components_db_entry = data[ comp ];

            let $parent = $(`#${parentElementId}`);
            let $components_div = _createListCategoryItem( $parent, `category-${category_idx}`, components_db_entry.label, components_db_entry.icon );
            $components_div.html('');

            _listComponents( $components_div, components_db_entry.components, category_idx );

            category_idx = category_idx + 1;
        }
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    let initComponentTreeToggle = function(){

        $('.collapse')
        .on('show.bs.collapse', function (el) {
            let $div = $(el.target);
            let div_id = $div.attr('id');
            // console.log(div_id)

            let $btn = $('[data-target="#'+div_id+'"]');
            $btn.toggleClass('active');
            let $icon = $btn.find('i');
            $icon.toggleClass('icon-chevron-right');
            $icon.toggleClass('icon-chevron-down');
        })
        .on('hide.bs.collapse', function (el) {
            let $div = $(el.target);
            let div_id = $div.attr('id');
            // console.log(div_id)

            let $btn = $('[data-target="#'+div_id+'"]');
            $btn.toggleClass('active');
            let $icon = $btn.find('i');
            $icon.toggleClass('icon-chevron-right');
            $icon.toggleClass('icon-chevron-down');
        });
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    var _graph = new joint.dia.Graph();

    let getGraph = function() {
        return _graph
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    var _selectedComponent = null;
    var _selectedAction = null;

    let getSelectedComponent = function() {
        return _selectedComponent
    };

    let setSelectedComponent = function( component ) {
        _selectedComponent = component;
    };

    let getSelectedAction = function() {
        return _selectedAction
    };

    let setSelectedAction = function( value ) {
        _selectedAction = value;
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    var CONFIG = {

        LIBRARY: {
            COMPONENT_TYPE_NAME: 'bpscript.Component',
            NATIVE_PRODUCER_TYPE_NAME: 'bpscript.NativeProducer',
            NATIVE_CONSUMER_TYPE_NAME: 'bpscript.NativeConsumer',
        },

        APP: {
            MODULE_NAME: 'BPScript',
        },

        PAPER: {
            HEADER_HEIGHT: 30,
            BACKGROUND_IMAGE: 'img/graph_bg.png',
        }
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    // JointJS adds elements and links to graph and
    // then adds graph to a paper.
    // There is no way to know to which paper a graph is added
    // if you only have graph reference.
    // We have only one paper instance (for now) in GUI so
    // we can get away with using this reference all around
    // the code base.
    let _paper = undefined;

    let getPaper$$1 = function() {
        return _paper
    };

    let _makePaper = function( options ) {

        var paperOpts = joint.util.assign({
            gridSize: 1,
            background: {
                image: CONFIG.PAPER.BACKGROUND_IMAGE,
                position: 'top left',
                repeat: 'repeat',
                opacity: 0.8
            },
            //
            // Configure links
            //
            linkPinning: false,
            defaultLink: new joint.shapes.standard.Link({
                connector: {
                    name: 'smooth',
                },
                attrs: {
                    line: { // selector for the visible <path> SVGElement
                        stroke: '#e7e7e7' // SVG attribute and value
                    }
                }
            }),

            validateConnection: function( cellViewS, magnetS, cellViewT, magnetT, end, linkView ) {
                // console.log("magnetS.getAttribute('port-group')", magnetS && magnetS.getAttribute('port-group'))
                // console.log("magnetT.getAttribute('port-group')", magnetT && magnetT.getAttribute('port-group'))

                // Prevent linking from output ports to input ports within one element.
                if( cellViewS === cellViewT ) return false;
                // Only allow links which end on input ports.
                return magnetT && magnetT.getAttribute('port-group') === 'inputs';
            },

            validateMagnet: function( cellView, magnet ) {
                // console.log("magnet.getAttribute('port-group')", magnet && magnet.getAttribute('port-group'))

                // Only allow linking interaction when starting link from output ports
                return magnet && magnet.getAttribute('port-group') === 'outputs';
            },

        }, options);

        var paper = new joint.dia.Paper( paperOpts );

        return paper
    };

    let _defineActionDragSelection = function( paper ) {

        var _selectedElements = [];

        let beginSelection = function( evt, x, y, model ) {

            let rectangle = new joint.shapes.standard.Rectangle({
                position: {
                    x: x,
                    y: y,
                },
                size: {
                    width: 2,
                    height: 2,
                },
                attrs: {
                    body: {
                        fill: 'none',
                        strokeDasharray: '5,5',
                    }
                },
            });

            rectangle.addTo( model );
            rectangle.toBack();

            evt.data = { rectangle: rectangle, x: x, y: y };
        };

        let doSelection = function( evt, x, y ) {

            let dx = x - evt.data.x;
            let dy = y - evt.data.y;
            evt.data.rectangle.resize( dx, dy );

            let currentlySelectedElements = [];
            let elements = getGraph().findModelsInArea( evt.data.rectangle.getBBox() );

            for( var idx in elements ) {

                let element = elements[ idx ];

                if( element == evt.data.rectangle ) continue
                currentlySelectedElements.push( element );

                let elementView = paper.findViewByModel( element );
                elementView.highlight();
            }

            let unselectedElements = joint.util.difference( _selectedElements, currentlySelectedElements );
            for( var idx in unselectedElements ) {

                let element = unselectedElements[ idx ];

                let elementView = paper.findViewByModel( element );
                elementView.unhighlight();
            }

            _selectedElements = currentlySelectedElements;
        };

        let endSelection = function( evt ) {

            let elements = getGraph().findModelsInArea( evt.data.rectangle.getBBox() );

            for( var idx in elements ) {
                let elementView = paper.findViewByModel( elements[ idx ] );
                elementView.unhighlight();
            }

            evt.data.rectangle.remove();
        };

        paper.on({

            ////////////////////////////////////////////////////////////////
            //
            // MULTI ELEMENT SELECTION SUPPORT
            //
            'blank:pointerdown': function( evt, x, y ) {
                // console.log('blank:pointerdown', evt.data)

                let action = getSelectedAction();
                // console.log('action', action)

                if( action == 'select' )
                    beginSelection( evt, x, y, this.model );

            },

            'blank:pointermove': function( evt, x, y ) {

                let action = getSelectedAction();
                // console.log('action', action)

                if( action == 'select' )
                    doSelection( evt, x, y );
            },

            'blank:pointerup': function( evt ) {
                // console.log('blank:pointerup', evt.data)

                let action = getSelectedAction();
                // console.log('action', action)

                if( action == 'select' )
                    endSelection( evt );
            },
            //
            ////////////////////////////////////////////////////////////////
        });
    };

    function _addElementToolsView$$1( elementView ) {

        //////////////////////////////////////////////////////////////////
        // START tools view

        // 1) creating element tools
        var removeTool = new BtnRemove({
            x: 10,
            // y: 0,
        });
        var infoTool = new BtnInfo({
            x: 25,
            // y: 0,
        });

        // 2) creating a tools view
        var toolsView = new joint.dia.ToolsView({
            name: 'basic-element-tools',
            tools: [removeTool, infoTool]
        });

        // 3) attaching to a link view
        elementView.addTools(toolsView);
        elementView.hideTools();

        // END tools view
        //////////////////////////////////////////////////////////////////
    }

    function _addLinkToolsView$$1( linkView ) {

        // add link tools
        // var verticesTool = new joint.linkTools.Vertices()
        // var segmentsTool = new joint.linkTools.Segments()
        // var boundaryTool = new joint.linkTools.Boundary()
        var removeTool = new joint.linkTools.Remove();

        // 2) creating a tools view
        var toolsView = new joint.dia.ToolsView({
            name: 'basic-link-tools',
            tools: [removeTool]
        });

        // 3) attaching to a link view
        // var linkView = link.findView(paper);
        linkView.addTools( toolsView );
        linkView.hideTools();
    }

    function _defineActionLinkToolsView( paper ) {

        paper.on({

            ////////////////////////////////////////////////////////////////
            //
            // LINK TOOLS
            //
            'link:connect' : function( linkView, evt, elementViewConnected, magnet, arrowhead ) {
                // console.log('link:connect', linkView, evt)
                _addLinkToolsView$$1( linkView );
            },
            'link:mouseenter': function( linkView ) {
                linkView.showTools();
            },
            'link:mouseleave': function( linkView ) {
                linkView.hideTools();
            },
            //
            ////////////////////////////////////////////////////////////////
        });
    }

    function _defineActionElementToolsView( paper ) {

        paper.on({

            ////////////////////////////////////////////////////////////////
            //
            // ELEMENT TOOLS
            //
            'element:mouseenter': function( elementView, evt ) {
                // console.log('element:mouseenter', elementView, evt)
                elementView.showTools();
            },

            'element:mouseleave': function( elementView, evt ) {
                // console.log('element:mouseleave', elementView, evt)
                elementView.hideTools();
            },
            //
            ////////////////////////////////////////////////////////////////
        });
    }

    let initPaper$$1 = function( canvasId ) {

        // TODO move references to DOM elements/selectors into their own module
        // and then pull them in via methods like getGraph() etc.
        _paper = _makePaper({
            el: document.getElementById( canvasId ),
            model: getGraph(),
            // width: '100%',
            // height: '100%', // bgnd is 128x128
        });

        getGraph().on('add', function(cell) { 
            // Only handle elements
            if( ! cell.isElement() ) return

            // Only handle bpscript.Component types
            if( cell.attributes.type != CONFIG.LIBRARY.COMPONENT_TYPE_NAME &&
                cell.attributes.type != CONFIG.LIBRARY.NATIVE_PRODUCER_TYPE_NAME &&
                cell.attributes.type != CONFIG.LIBRARY.NATIVE_CONSUMER_TYPE_NAME ) return

            // console.log( 'cell', cell )
            cell.adjustViewSize( _paper );
        });

        _paper.on('render:done', function(evt, x, y) {
            console.log('render:done.');
        });

        _defineActionDragSelection( _paper );
        _defineActionElementToolsView( _paper );
        _defineActionLinkToolsView( _paper );
    };

    let clearPaper$$1 = function() {
        getGraph().clear();
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    function dragOverHandler$$1(ev) {
        // console.log('dragOverHandler',ev)
        ev.preventDefault();
    }

    function dragStartHandler$$1(ev) {
        // console.log('dragStartHandler',ev)
        // console.log('ev.srcElement',ev.srcElement)
        let $button = $(ev.srcElement);
        // console.log('$button',$button)
        // console.log('$button.data(\'component\')',$button.data('component'))
        setSelectedComponent( $button.data('component') );
        // console.log('selectedComponent',getSelectedComponent())

        // var img = new Image()
        var img = document.createElement("img");
        let icon_src = $('#dnd-handler img').attr('src') || 'icons/box.svg';
        img.src = icon_src;
        // console.log('img', img)

        ev.dataTransfer.setDragImage(img, 24, 24);
    }


    function dropHandler$$1(ev) {
        // console.log('dropHandler',ev.layerX, ev.layerY)
        ev.preventDefault();
        // var data = ev.dataTransfer.getData("text")
        // console.log('dropHandler -> data',data)

        if( getSelectedComponent() ) {

            var newNode = makeNetworkNode( getSelectedComponent() );

            newNode.position(ev.layerX, ev.layerY);
            newNode.resize(120, 80);
            newNode.addTo( getGraph() );
            // FIXME: we are referencing a global paper instance here!
            // we should probably create and add this somewhere else or what?
            var componentView = newNode.findView( getPaper$$1() );
            _addElementToolsView$$1( componentView );

            setSelectedComponent( null );
        }
    }

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */


    const ModalConfirmation = function( id, message ) {

        const self = this;

        this.template = [
            '<div class="modal fade" id="'+id+'" tabindex="-1" role="dialog" aria-labelledby="'+id+'Label" aria-hidden="true">',
            '  <div class="modal-dialog modal-sm modal-dialog-centered" role="document">',
            '    <div class="modal-content">',
            '      <div class="modal-header">',
            '        <h5 class="modal-title" id="'+id+'Label">Confirm</h5>',
            '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">',
            '          <span aria-hidden="true">&times;</span>',
            '        </button>',
            '      </div>',
            '      <div class="modal-body">',
            message,
            '      </div>',
            '      <div class="modal-footer">',
            '        <button type="button" class="btn btn-secondary" data-dismiss="modal">Nah, I changed my mind</button>',
            '        <button type="button" class="btn btn-primary btn_modal_action">Do it</button>',
            '      </div>',
            '    </div>',
            '  </div>',
            '</div>',
        ].join('');
        
        this.show = function( actionCallback ) {
            self.actionCallback = actionCallback;
            $('#'+id).modal({});
        },

        this.action = function() {
            $('#'+id).modal('hide');
            if( self.actionCallback ) {
                self.actionCallback();
                self.actionCallback = undefined;
            }
        };

        $('body').append($(this.template));

        $('#'+id).find('.btn_modal_action').on('click', function(ev) {
            self.action();
        });
    };

    const ModalNetworkExport = function( id ) {

        const self = this;

        this.template = [
            '<div class="modal fade" id="'+id+'" tabindex="-1" role="dialog" aria-labelledby="'+id+'Label" aria-hidden="true">',
            '  <div class="modal-dialog modal-dialog-centered" role="document">',
            '    <div class="modal-content">',
            '      <div class="modal-header">',
            '        <h5 class="modal-title" id="'+id+'Label">BPNET export</h5>',
            '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">',
            '          <span aria-hidden="true">&times;</span>',
            '        </button>',
            '      </div>',
            '      <div class="modal-body">',
            '         <div class="form-group">',
            '           <label for="'+id+'_textarea">Source</label>',
            '           <textarea class="form-control bpnet-source-value" id="'+id+'_textarea" rows="10" style="font-family: monospace;"></textarea>',
            '         </div>',
            '      </div>',
            '      <div class="modal-footer">',
            // '        <button type="button" class="btn btn-secondary" data-dismiss="modal">Nah, I changed my mind</button>',
            '        <button type="button" class="btn btn-primary btn_modal_action">Nice</button>',
            '      </div>',
            '    </div>',
            '  </div>',
            '</div>',
        ].join('');
        
        this.show = function( content ) {
            $('#'+id).find('.bpnet-source-value').val(content);
            $('#'+id).modal({});
        },

        this.action = function() {
            $('#'+id).modal('hide');
            $('#'+id).find('.bpnet-source-value').val('');
        };

        $('body').append($(this.template));

        $('#'+id).find('.btn_modal_action').on('click', function(ev) {
            self.action();
        });
    };

    exports.modalConfirmDeleteAll = undefined;
    exports.modalConfirmDeleteNode = undefined;

    exports.modalNetworkExport = undefined;

    let initModals = function() {

        exports.modalConfirmDeleteAll = new ModalConfirmation('modalConfirmDeleteAll', 'Delete all nodes in this network?');
        exports.modalConfirmDeleteNode = new ModalConfirmation('modalConfirmDeleteNode', 'Delete selected nodes?');

        exports.modalNetworkExport = new ModalNetworkExport('modalNetworkExport', '');
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */


    let ModalComponentNodeDetails = function( id ) {

        const self = this;

        this.template = [
            '<div class="modal fade" id="'+id+'" tabindex="-1" role="dialog" aria-labelledby="'+id+'Label" aria-hidden="true">',
            '  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">',
            '    <div class="modal-content">',
            '      <div class="modal-header">',
            '        <h5 class="modal-title" id="'+id+'Label">Native Node Details</h5>',
            '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">',
            '          <span aria-hidden="true">&times;</span>',
            '        </button>',
            '      </div>',
            '      <div class="modal-body">',
            '         <div class="form-group">',
            '           <label for="'+id+'_input">Label</label>',
            '           <input type="text" class="form-control component-node-label" id="'+id+'_input">',
            '         </div>',
            '      </div>',
            '      <div class="modal-footer">',
            '        <button type="button" class="btn btn-secondary" data-dismiss="modal">Nah, I changed my mind</button>',
            '        <button type="button" class="btn btn-primary btn_modal_action">Do it</button>',
            '      </div>',
            '    </div>',
            '  </div>',
            '</div>',
        ].join('');
        
        this.show = function( data, actionCallback ) {
            self.actionCallback = actionCallback;

            let message = 'type -> ' + data.type + '<br>';
            message += 'data ' + JSON.stringify(data);

            $('#'+id).find('.component-node-label').val(data.label || '[no label set]');
            $('#'+id).modal({});
        },

        this.action = function() {
            let data = {
                label: $('#'+id).find('.component-node-label').val()
            };
            $('#'+id).modal('hide');
            if( self.actionCallback ) {
                self.actionCallback(data);
                self.actionCallback = undefined;
            }
        };

        $('body').append($(this.template));

        $('#'+id).find('.btn_modal_action').on('click', function(ev) {
            self.action();
        });
    };

    let ModalNativeNodeDetails = function( id ) {

        const self = this;

        this.template = [
            '<div class="modal fade" id="'+id+'" tabindex="-1" role="dialog" aria-labelledby="'+id+'Label" aria-hidden="true">',
            '  <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">',
            '    <div class="modal-content">',
            '      <div class="modal-header">',
            '        <h5 class="modal-title" id="'+id+'Label">Component Node Options</h5>',
            '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">',
            '          <span aria-hidden="true">&times;</span>',
            '        </button>',
            '      </div>',
            '      <div class="modal-body">',
            '         <div class="form-group">',
            '           <label for="'+id+'_textarea">Config Buffer</label>',
            '           <textarea class="form-control config-buffer-value" id="'+id+'_textarea" rows="3"></textarea>',
            '         </div>',
            '      </div>',
            '      <div class="modal-footer">',
            '        <button type="button" class="btn btn-secondary" data-dismiss="modal">Leave it as is</button>',
            '        <button type="button" class="btn btn-primary btn_modal_action">Change it</button>',
            '      </div>',
            '    </div>',
            '  </div>',
            '</div>',
        ].join('');
        
        this.show = function( data, actionCallback ) {
            self.actionCallback = actionCallback;
            $('#'+id).find('.config-buffer-value').val(data.configBuffer || '');
            $('#'+id).modal({});
        },

        this.action = function() {
            let data = {
                configBuffer : $('#'+id).find('.config-buffer-value').val()
            };
            $('#'+id).modal('hide');
            if( self.actionCallback ) {
                self.actionCallback(data);
                self.actionCallback = undefined;
            }
        };

        $('body').append($(this.template));

        $('#'+id).find('.btn_modal_action').on('click', function(ev) {
            self.action();
        });
    };


    exports.editModalComponentNodeDetails = undefined;
    exports.editModalNativeNodeDetails = undefined;

    let initEdits = function() {

        exports.editModalComponentNodeDetails = new ModalComponentNodeDetails('editModalComponentNodeDetails');
        exports.editModalNativeNodeDetails = new ModalNativeNodeDetails('editModalNativeNodeDetails');
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    const BtnRemove = joint.elementTools.Button.extend({
        children: [{
            tagName: 'circle',
            selector: 'button',
            attributes: {
                'r': 7,
                'fill': '#DF691A',
                'cursor': 'pointer'
            }
        }, {
            tagName: 'path',
            selector: 'icon',
            attributes: {
                'd': 'M -3 -3 3 3 M -3 3 3 -3',
                'fill': 'none',
                'stroke': '#FFFFFF',
                'stroke-width': 2,
                'pointer-events': 'none'
            }
        }],
        options: {
            distance: 60,
            offset: 0,
            action: function(evt, view, tool) {
                exports.modalConfirmDeleteNode.show(function(){
                    view.model.remove({ ui: true, tool: tool.cid });
                });
            }
        }
    });

    const BtnInfo = joint.elementTools.Button.extend({
        children: [{
            tagName: 'circle',
            selector: 'button',
            attributes: {
                'r': 7,
                'fill': '#959EA7',
                'cursor': 'pointer'
            }
        }, {
            tagName: 'path',
            selector: 'icon',
            attributes: {
                'd': 'M -2 4 2 4 M 0 3 0 0 M -2 -1 1 -1 M -1 -4 1 -4',
                'fill': 'none',
                'stroke': '#FFFFFF',
                'stroke-width': 2,
                'pointer-events': 'none'
            }
        }],
        options: {
            // x: '100%',
            // y: 0,
            action: function(evt) {
                // let message = 'View id: ' + this.id + '<br>' + 'Model id: ' + this.model.id + '<br>'
                let bpcomponent = this.model.get('bpcomponent');
                console.log('what have we got here',bpcomponent);

                if( bpcomponent.type == 'component' ) {

                    exports.editModalComponentNodeDetails.show( bpcomponent, function(data) {
                        bpcomponent.label = data.label;
                        console.log('the right stuff',bpcomponent);
                    });

                } else if( bpcomponent.type == 'nativeProducer' || bpcomponent.type == 'nativeConsumer' ) {

                    exports.editModalNativeNodeDetails.show( bpcomponent, function(data) {
                        let cb = data.configBuffer.split('\n').map(el => ' '+el.trim()).filter( el => el.trim() ).join('\n');
                        bpcomponent.configBuffer = cb;
                        console.log('the right stuff',bpcomponent);
                    });
                }
            }
        }
    });


    let inputPortsGroup = {
        position: {
            // name: 'left', // layout name
            // args: {
            //     dy: 10,
            // }, // arguments for port layout function, properties depends on type of layout
            name: 'line',
            args: {
                start: { x: 0, y: CONFIG.PAPER.HEADER_HEIGHT },
                end: { x: 0, y: '100%' },
            },
        },
        label: {
            position: {
                name: 'right',
                args: { y: 0 } // extra arguments for the label layout function, see `layout.PortLabel` section
            },
            markup: [{
                tagName: 'text',
                selector: 'text',
                className: 'label-text',
            }],
        },
        markup: [{
            tagName: 'circle',
            selector: 'circle',
        }],
        attrs: {
            circle: {
                r: 8,
                stroke: '#00a8f4',
                strokeWidth: 2,
                fill: '#292929',
                magnet: true,
            },
            text: {
                fontFamily: 'Arial, Helvetica, sans-serif',
                fontSize: 'smaller',
                fill: '#e7e7e7',
            }
        },
    };

    let outputPortsGroup = {
        position: {
            // name: 'right', // layout name
            // args: {
            //     dy: 10,
            // }, // arguments for port layout function, properties depends on type of layout
            name: 'line',
            args: {
                start: { x: '100%', y: CONFIG.PAPER.HEADER_HEIGHT },
                end: { x: '100%', y: '100%' },
            },
        },
        label: {
            position: {
                name: 'left',
                args: { y: 0 } // extra arguments for the label layout function, see `layout.PortLabel` section
            },
            markup: [{
                tagName: 'text',
                selector: 'text',
                className: 'label-text',
            }],
        },
        markup: [{
            tagName: 'circle',
            selector: 'circle',
        }],
        attrs: {
            circle: {
                r: 8,
                stroke: '#00a8f4',
                strokeWidth: 2,
                fill: '#292929',
                magnet: true,
            },
            text: {
                fontFamily: 'Arial, Helvetica, sans-serif',
                fontSize: 'smaller',
                fill: '#e7e7e7',
            }
        },
    };

    const BPScriptComponent = 
    joint.dia.Element.define(CONFIG.LIBRARY.COMPONENT_TYPE_NAME, {
        //
        // overrides for default attributes
        //
        attrs: {
            body: {
                refWidth: '100%',
                refHeight: '100%',
                strokeWidth: 2,
                stroke: '#000000',
                fill: '#232323',
                rx: 5,
                ry: 5,
            },
            bodyText: {
                textVerticalAnchor: 'middle',
                textAnchor: 'middle',
                refX: '50%',
                refY: '50%',
                refY2: 15,
                fontFamily: 'Arial, Helvetica, sans-serif',
                fontSize: 14,
                fill: '#333333'
            },
            header: {
                refWidth: '100%',
                height: CONFIG.PAPER.HEADER_HEIGHT,
                strokeWidth: 2,
                stroke: '#000000',
                fill: '#3e3e3e',
                rx: 5,
                ry: 5,
            },
            headerText: {
                textVerticalAnchor: 'middle',
                textAnchor: 'middle',
                refX: '50%',
                refY: 15,
                fontFamily: 'Arial, Helvetica, sans-serif',
                fontSize: 16,
                fill: '#e7e7e7'
            },
        },
        ports: {
            groups: {
                'inputs': inputPortsGroup,
                'outputs': outputPortsGroup,
            },
            items: []
        },
        bpcomponent: {
        }
        // portMarkup: '<rect width="20" height="20" fill="black"/>',
    }, {
        //
        // prototype properties
        //
        markup: [{
            tagName: 'rect',
            selector: 'body',
        }, {
            tagName: 'rect',
            selector: 'header'
        }, {
            tagName: 'text',
            selector: 'headerText'
        }, {
            tagName: 'text',
            selector: 'bodyText'
        }],

        addInPort: function( portIndex, portLabel, portColor = '#00a8f4' ) {
            // console.log('this', this)

            var port = {
                // id: 'abc', // generated if `id` value is not present
                group: 'inputs',
                args: {}, // extra arguments for the port layout function, see `layout.Port` section
                label: {
                    // ....
                },
                attrs: {
                    text: {
                        text: portLabel
                    },
                    circle: {
                        stroke: portColor,
                    }
                },
                bpport: {
                    index: portIndex,
                    name: portLabel,
                },
            };

            return this.addPort(port)
        },

        addOutPort: function( portIndex, portLabel, portColor = '#00a8f4' ) {
            // console.log('this', this)

            var port = {
                // id: 'abc', // generated if `id` value is not present
                group: 'outputs',
                args: {}, // extra arguments for the port layout function, see `layout.Port` section
                label: {
                    // ....
                },
                attrs: {
                    text: {
                        text: portLabel
                    },
                    circle: {
                        stroke: portColor,
                    }
                },
                bpport: {
                    index: portIndex,
                    name: portLabel,
                },
            };

            return this.addPort(port)
        },

        adjustViewSize: function( paper ) {

            var cellView = this.findView( paper );

            let padding = 20;
            var cellWidth = this.attributes.size.width;

            let $headerText = cellView.$('[joint-selector="headerText"]');
            let headerTextWidth = $headerText[0].getBoundingClientRect().width + padding;

            cellWidth = headerTextWidth > cellWidth ? headerTextWidth : cellWidth;

            let $ports = cellView.$('.joint-port');
            var maxPortWidth = 0;

            $ports.each(function(idx, $el) {

                let portWidth = $el.getBoundingClientRect().width;
                if( portWidth > maxPortWidth ) {
                    maxPortWidth = portWidth;
                }
            });

            maxPortWidth = maxPortWidth * 2 + padding;
            cellWidth = maxPortWidth > headerTextWidth ? maxPortWidth : headerTextWidth;

            cellWidth = Math.ceil( cellWidth );
            let cellHeight = this.attributes.size.height;

            return this.resize( cellWidth, cellHeight )
        },
    });

    const BPScriptNativeConsumer = 
    BPScriptComponent.define(CONFIG.LIBRARY.NATIVE_CONSUMER_TYPE_NAME, {
        //
        // overrides for default attributes
        //
        attrs: {
            header: {
                fill: '#959595',
            },
            body: {
                fill: '#6b6b6b',
            },
        },
    });

    const BPScriptNativeProducer = 
    BPScriptComponent.define(CONFIG.LIBRARY.NATIVE_PRODUCER_TYPE_NAME, {
        //
        // overrides for default attributes
        //
        attrs: {
            header: {
                fill: '#959595',
            },
            body: {
                fill: '#6b6b6b',
            },
        },
    });

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    let _makeComponent = function( options ) {

        let headerLabel = options.label || 'component';
        let inputs = options.inputs || [];
        let outputs = options.outputs || [];

        var component = new BPScriptComponent({
            ports: {
                items: []
            },
            bpcomponent: joint.util.assign( {}, options )
        });

        component.attr({
            headerText: {
                text: headerLabel,
            },
        });

        for( var idx in inputs) {

            let element = inputs[ idx ];
            let label = element.name || 'IN';
            let color = element.pinColor || 'white';

            component.addInPort( idx, label, color );
        }

        for( var idx in outputs) {

            let element = outputs[ idx ];
            let label = element.name || 'OUT';
            let color = element.pinColor || 'white';

            component.addOutPort( idx, label, color );
        }

        return component
    };

    let _makeConsumer = function( options ) {

        let headerLabel = options.label || 'native consumer';

        let component = new BPScriptNativeConsumer({
            ports: {
                items: []
            },
            bpcomponent: joint.util.assign( {}, options )
        });

        component.attr({
            headerText: {
                text: headerLabel,
            },
        });

        let label = options.port.name || 'IN';
        let color = options.port.pinColor || 'white';
        component.addInPort( 0, label, color );

        return component
    };

    let _makeProducer = function( options ) {

        let headerLabel = options.label || 'native producer';

        let component = new BPScriptNativeProducer({
            ports: {
                items: []
            },
            bpcomponent: joint.util.assign( {}, options )
        });

        component.attr({
            headerText: {
                text: headerLabel,
            },
        });

        let label = options.port.name || 'OUT';
        let color = options.port.pinColor || 'white';
        component.addOutPort( 0, label, color );

        return component
    };

    let makeNetworkNode = function( options ) {

        options = options || {};
        let args = joint.util.assign( {}, options );
        // console.log('args', args)

        if( args.type == 'component' ) {
            return _makeComponent( args )
        }

        if( args.type == 'nativeProducer' ) {
            return _makeProducer( args )
        }

        if( args.type == 'nativeConsumer' ) {
            return _makeConsumer( args )
        }
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    let initDOM = function() {
        initModals();
        initEdits();
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */


    let getJSON = function( url ) {

        return $.get({
            dataType: 'json',
            success: null,
            url: url
        })
        .fail( function( jqXHR, textStatus, errorThrown ) {
            console.error( `GET "${url}" -> ${jqXHR.status} - '${errorThrown}'` );
        })
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    let _loadDatabaseComponents = function( databaseRootUrl, dbData ) {

        // In case dbData is empty
        // we want to return a valid promise which is already resolved
        var cpd = $.Deferred();
        cpd.resolve();
        var currentPromise = cpd.promise();

        var previousPromise = null;

        for( var comp in dbData ) {

            let components_db_entry = dbData[ comp ];

            currentPromise =
            getJSON( databaseRootUrl + '/' + components_db_entry.source + '/components.json' )
            .done( function( componentsData, textStatus, jqXHR ) {

                for( var comp in componentsData ) {

                    let component = componentsData[comp];
                    component.name = component.source;
                    component.source = components_db_entry.source + '/' + component.source;
                }

                components_db_entry.components = componentsData;
            })
            .then( previousPromise );
            previousPromise = currentPromise;
        }

        return currentPromise
    };

    let loadDatabase = function( databaseRootUrl ) {

        let dfd = $.Deferred();

        getJSON( databaseRootUrl + '/db.json' )
        .done( function( data, textStatus, jqXHR ) {
            _loadDatabaseComponents( databaseRootUrl, data )
            .always(function() {
                dfd.resolve( data );
            });
        });

        return dfd.promise()
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    let _attachToolViews = function() {
        let graph = getGraph();
        let nodes = graph.getElements();
        let connections = graph.getLinks();
        for( var idx in nodes ) {
            let node = nodes[idx];
            let nodeView = node.findView( getPaper$$1() );
            _addElementToolsView$$1( nodeView );
        }
        for( var idx in connections ) {
            let connection = connections[idx];
            let connectionView = connection.findView( getPaper$$1() );
            _addLinkToolsView$$1( connectionView );
        }
    };

    let _generateNetSource = function() {
        let graph = getGraph();
        let nodes = graph.getElements();
        let connections = graph.getLinks();

        let output = [];
        let buffers_source = [];
        let contexts_source = [];
        let connections_source = [];

        let nativeFunctionsUsed = new Set();

        for( var idx in nodes ) {
            let node = nodes[idx];
            // console.log( node )
            // console.log( node.getPorts() )
            let bpcomponent = node.get('bpcomponent');
            // console.log( 'type', bpcomponent.type )
            switch( bpcomponent.type ) {
                case 'component':
                    contexts_source.push([
                        '[', bpcomponent.name, ']',
                        '<--',
                        bpcomponent.source.replace('/','.'),
                        ''
                    ].join(' '));
                    break;
                case 'nativeConsumer':
                case 'nativeProducer':
                    let bufferName = 'buffer_' + idx;
                    bpcomponent.bufferName = bufferName;

                    buffers_source.push([
                        '{ ', bufferName, ' }', '<--\n',
                        bpcomponent.configBuffer, '\n',
                        'EOB <--',
                        ''
                    ].join(''));

                    if( ! nativeFunctionsUsed.has( bpcomponent.nativeFunction ) ) {

                        nativeFunctionsUsed.add( bpcomponent.nativeFunction );

                        contexts_source.push([
                            '<', bpcomponent.nativeFunction, '>',
                            '<--',
                            bpcomponent.source.replace('/','.'),
                            ''
                        ].join(' '));
                    }
                    break;
            }
        }

        for( var idx in connections ) {
            let conn = connections[idx];
            let linkSource = conn.get('source');
            let linkTarget = conn.get('target');
            let sourceElement = conn.getSourceElement();
            let targetElement = conn.getTargetElement();
            let sourcePort = sourceElement.getPort( linkSource.port );
            let targetPort = targetElement.getPort( linkTarget.port );
            let sourceBPComponent = sourceElement.get('bpcomponent');
            let targetBPComponent = targetElement.get('bpcomponent');

            if( sourceBPComponent.type == 'nativeProducer' ) {

                if( targetBPComponent.type == 'nativeConsumer' ) {

                    connections_source.push([
                        '<', sourceBPComponent.nativeFunction, ',', sourceBPComponent.bufferName, '>',
                        '-->', 
                        '<', targetBPComponent.nativeFunction, ',', targetBPComponent.bufferName, '>',
                        ''
                    ].join(' '));

                } else {

                    connections_source.push([
                        '<', sourceBPComponent.nativeFunction, ',', sourceBPComponent.bufferName, '>',
                        '-->', 
                        targetPort.bpport.index, ':', '[', targetBPComponent.name, ']',
                        ''
                    ].join(' '));
                }

            } else if( targetBPComponent.type == 'nativeConsumer' ) {

                connections_source.push([
                    '[', sourceBPComponent.name, ']', ':', sourcePort.bpport.index, 
                    '-->', 
                    '<', targetBPComponent.nativeFunction, ',', targetBPComponent.bufferName, '>',
                    ''
                ].join(' '));

            } else {

                connections_source.push([
                    // sourceBPComponent.source, ':', 
                    '[', sourceBPComponent.name, ']', ':', 
                    // sourcePort.bpport.name, 
                    sourcePort.bpport.index, 
                    '-->', 
                    // targetPort.bpport.name,
                    targetPort.bpport.index,
                    ':',
                    // targetBPComponent.source,
                    '[', targetBPComponent.name, ']',
                    ''
                ].join(' '));
            }
            // console.log( linkSource )
            // console.log( sourcePort.bpport )
            // console.log( linkTarget )
            // console.log( targetPort.bpport )
        }

        output.push( '; buffers' );
        output.push( buffers_source.join('\n') );
        output.push( '; contexts' );
        output.push( contexts_source.join('\n') );
        output.push( '; connections' );
        output.push( connections_source.join('\n') );

        let outputString = output.join('\n');
        // console.log( outputString )

        return outputString
    };

    let exportNet = function() {
        // TODO move these 2 lines to somewhere more appropriate
        let bpnetSource = _generateNetSource();
        exports.modalNetworkExport.show( bpnetSource );

        return getGraph().toJSON()
    };

    let importNet = function( data ) {
        getGraph().fromJSON( data );
        _attachToolViews();
    };

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    var REVISION = '1dev';

    /* Copyright 2019 Ravendyne Inc. */
    /* SPDX-License-Identifier: GPL-3.0-or-later */

    // import { version } from '../package.json'

    $(function(){
    });

    exports.displayComponentDatabase = displayComponentDatabase$$1;
    exports.initComponentTreeToggle = initComponentTreeToggle;
    exports.dragStartHandler = dragStartHandler$$1;
    exports.dragOverHandler = dragOverHandler$$1;
    exports.dropHandler = dropHandler$$1;
    exports.initModals = initModals;
    exports.initEdits = initEdits;
    exports.initPaper = initPaper$$1;
    exports.clearPaper = clearPaper$$1;
    exports.getPaper = getPaper$$1;
    exports._addElementToolsView = _addElementToolsView$$1;
    exports._addLinkToolsView = _addLinkToolsView$$1;
    exports.makeNetworkNode = makeNetworkNode;
    exports.BPScriptComponent = BPScriptComponent;
    exports.BPScriptNativeConsumer = BPScriptNativeConsumer;
    exports.BPScriptNativeProducer = BPScriptNativeProducer;
    exports.BtnRemove = BtnRemove;
    exports.BtnInfo = BtnInfo;
    exports.getSelectedComponent = getSelectedComponent;
    exports.setSelectedComponent = setSelectedComponent;
    exports.getSelectedAction = getSelectedAction;
    exports.setSelectedAction = setSelectedAction;
    exports.initDOM = initDOM;
    exports.getJSON = getJSON;
    exports.loadDatabase = loadDatabase;
    exports.exportNet = exportNet;
    exports.importNet = importNet;
    exports.REVISION = REVISION;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
