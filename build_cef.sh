#!/bin/bash

rm -rf build
find . -name "*pyc" -delete
find . -name "__pycache__" -delete

cd jssrc
npm install
npm run build
cd ..

mkdir build
cp -r pysrc/* build
cp -r html build
cp -r components build/html
