/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/core/information_packet.h"
#include "vm/interpreter/data_access.h"

#include "vm/lib/memory.h"

#include "vm/native.h"


static void sendConfigBufferToConnection( FBNativeContextHandle handle ) {

    byte_t *configBuffer = FB_GET_BUFFER_FROM_HANDLE( handle );
    size_t configBufferSize = FB_GET_BUFFER_SIZE_FROM_HANDLE( handle );

    FBInformationPacketTuple tuple = fbInformationPacket_create_tuple( configBufferSize );

    FB_COPY_ARRAY( tuple.buffer, configBuffer, byte_t, configBufferSize );

    bool writeSuccess = fbNativeConnection_write( handle, tuple.handle );
}

void sendValueOnce( FBNativeContextHandle handle ) {

    if( FB_GET_IS_TERMINATED_FROM_HANDLE( handle ) ) return;
    // or
    // if( fbNativeConnection_isTerminated( handle ) ) return;

    sendConfigBufferToConnection( handle );

    fbNativeConnection_close( handle );
}

void sendValue( FBNativeContextHandle handle ) {

    if( FB_GET_IS_TERMINATED_FROM_HANDLE( handle ) ) return;
    // or
    // if( fbNativeConnection_isTerminated( handle ) ) return;

    sendConfigBufferToConnection( handle );
}
