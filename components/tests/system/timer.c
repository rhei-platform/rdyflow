/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/native.h"

#include "vm/core/information_packet.h"
#include "vm/interpreter/data_access.h"

#include "vm/lib/memory.h"

#include <time.h>
#include <stdio.h>


void simpleBusyTimerFn( FBNativeContextHandle handle ) {
    // printf("simpleBusyTimerFn called\n");

    struct timespec *tag = FB_GET_TAG_FROM_HANDLE( handle );

    // if( fbNativeConnection_isTerminated( handle ) ) {
    // or
    if( FB_GET_IS_TERMINATED_FROM_HANDLE( handle ) ) {
         if( tag != NULL ) {
            FB_FREE_ARRAY( tag );
            FB_SET_TAG_FROM_HANDLE( handle, NULL );
         }
        return;
    }

    if( tag == NULL ) {

        tag = FB_GROW_ARRAY( tag, struct timespec, 1 );

        FB_SET_TAG_FROM_HANDLE( handle, tag );

        if( clock_gettime( CLOCK_REALTIME, tag ) ) {
            printf("Error!\n");
            return;
        }
    }

    struct timespec current;
    if( clock_gettime( CLOCK_REALTIME, &current ) ) {
        printf("Error!\n");
        return;
    }

    long prev_time = tag->tv_sec * 1000 + tag->tv_nsec / 1000000;
    long now_time = current.tv_sec * 1000 + current.tv_nsec / 1000000;
    long time_diff = now_time - prev_time;

    if( time_diff >= 1000 ) { // one sec

        printf("%ld ms\n", now_time);

        FB_COPY_ARRAY( tag, &current, struct timespec, 1 );

        FBInformationPacketTuple tuple = fbInformationPacket_create_tuple( sizeof( int32_t ) );
        fbData_set_int32( tuple.buffer, 42 );
        bool writeSuccess = fbNativeConnection_write( handle, tuple.handle );
    }
}
