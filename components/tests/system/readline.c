/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/core/information_packet.h"
#include "vm/core/scheduler.h"
#include "vm/interpreter/data_access.h"
#include "vm/core/connection.h"

#include "vm/debug/vm_debug.h"

#include "vm/native.h"

#include <stdio.h>

int32_t fnInputValue;

void inputFn( FBNativeContextHandle handle ) {

    if( FB_GET_IS_TERMINATED_FROM_HANDLE( handle ) ) return;
    // or
    // if( fbNativeConnection_isTerminated( handle ) ) return;

    enum inputState {
        IFN_STATE_WRITE,
        IFN_STATE_TERMINATE,
        IFN_STATE_DONE
    };

    static FBInformationPacketHandle packet = FB_INVALID_PACKET_HANDLE;
    static enum inputState state = IFN_STATE_WRITE;

    switch( state )
    {
        case IFN_STATE_WRITE: {

            FBInformationPacketTuple tuple = fbInformationPacket_create_tuple( sizeof( int32_t ) );
            packet = tuple.handle;
            fbData_set_int32( tuple.buffer, fnInputValue );

            bool writeSuccess = fbNativeConnection_write( handle, packet );
            if( writeSuccess ) {
                packet = FB_INVALID_PACKET_HANDLE;
                state = IFN_STATE_TERMINATE;
            }
        }
            break;

        case IFN_STATE_TERMINATE: {

            fbNativeConnection_close( handle );
            state = IFN_STATE_DONE;
        }
            break;

        default:
            break;
    }
}
