/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/native.h"

#include "vm/core/information_packet.h"
#include "contrib/buffer_printf.h"

#include <stdio.h>


void stdoutPrint( FBNativeContextHandle handle ) {
    // printf("stdoutPrint called\n");

    if( FB_GET_IS_TERMINATED_FROM_HANDLE( handle ) ) return;
    // or
    // if( fbNativeConnection_isTerminated( handle ) ) return;

    FBInformationPacketHandle ip = fbNativeConnection_read( handle );

    if( ip != FB_INVALID_PACKET_HANDLE ) {

        byte_t* buffer = fbInformationPacket_getDataBuffer( ip );
        size_t buffer_size = fbInformationPacket_getDataBufferSize( ip );

        const char *fmt = (const char*)FB_GET_BUFFER_FROM_HANDLE( handle );

        FBByteBuffer out_b;
        FBByteBuffer *output = &out_b;
        fbByteBuffer_init( output );

        // printf( "fmt = '%s'\n", fmt );
        // printf( "buffer = '%s', [%d]\n", (char*)buffer, buffer_size );
        fbContrib_printf( output, fmt, buffer, buffer_size );

        fbByteBuffer_addValue( output, 0 );
        // printf( "output->buffer = '%s'\n", (char*)output->buffer );
        printf( "%s", (char*)output->buffer );

        fbByteBuffer_free( output );
        fbInformationPacket_drop( ip );
    }
}
