- [] add network rules checker as a separate module
    - [] have others check with the checker if they can do things or not, i.e.
        call checker.canConnect( portA, portB ) to see if we can connect two ports

- [] cefpython3 version
    - [] handling of file loading
        - [] components tree(s)
        - [] for compilers input and output
    - [] do bpnet build with file output to designated folder
    - [] run scons compiler on bpnet build output

- [x] add port index to input/output definition in json    
- [] use portIndex property to convert port name to specific port index


- [] fix size on high-dpi displays (it's micro-miniature!!)
