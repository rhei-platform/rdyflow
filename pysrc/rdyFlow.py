## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

########################################################################
#
#   Python 3+ needs this
#
########################################################################
import sys
import os

_FILE_LOCATION = os.path.dirname( os.path.realpath( __file__ ) )
_LIB_LOCATION = os.path.normpath( os.path.join( _FILE_LOCATION, "./lib" ) )
sys.path.append( _LIB_LOCATION )
########################################################################



import check_deps

from cefpython3 import cefpython as cef

from lib.cef_app import cef_app
from lib.utils import Console


########################################################################
#
#   Application specific
#
########################################################################

class JSConsole(object):

    def __init__(self, browser):
        self.browser = browser
 
    def log(self, *args):
        format_string = ''
        for a in args:
            format_string += " {}"
            # print('type', type(a), 'value', a)
            # if type(a) == str:
            #     format_string += " '{:s}'"
            # else:
            #     format_string += " {}"
        format_string = "[console.log]" + format_string
        Console().info(format_string.format(*args))

    def error(self, *args):
        format_string = ''
        for a in args:
            format_string += " {}"
        format_string = "[console.error]" + format_string
        Console().error(format_string.format(*args))



class MyAppAPI(object):
    def __init__(self, browser):
        self.browser = browser
    
    def button_click(self, data):
        Console().info('Button clickd!!! "{}"'.format(data))
    
    def do_and_return(self, on_success=None, on_error=None):
        Console().info('do_and_return!!!')
        if on_success:
            on_success.Call("SUCCESS data")
        if on_error:
            on_error.Call("ERROR data")



def initialize_app(browser):
    myapp_api = MyAppAPI(browser)
    js_console = JSConsole(browser)

    bindings = cef.JavascriptBindings(bindToFrames=False, bindToPopups=False)

    bindings.SetProperty("python_property", "This property was set in Python")
    bindings.SetProperty("cefpython_version", cef.GetVersion())
    bindings.SetObject("myapp_api", myapp_api)
    bindings.SetObject("console", js_console)

    browser.SetJavascriptBindings(bindings)


########################################################################
#
########################################################################


if __name__ == '__main__':
    Console.level = Console.DEBUG
    Console().info('Starting...')

    app_settings = {
        "window_title": "rdyFlow - Rhei Platform GUI",
        "window_width": 1280,
        "window_height": 800,
    }

    # cef_app( initialize_app, 'index.html', app_settings )
    cef_app(
        app_init_callback = initialize_app,
        index_page = 'index.html',
        app_settings = app_settings
    )

    Console().info('Done.')
