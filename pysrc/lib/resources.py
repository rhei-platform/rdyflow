## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from cefpython3 import cefpython

import os
import time
import mimetypes

from utils import Console


# https://github.com/cztomczak/cefpython/blob/master/api/LocalResourceHandler.md#resourcehandler-interface
class LocalResourceHandler:

    # The methods of this class will always be called
    # on the IO thread.

    _resourceHandlerId = None
    _clientHandler = None
    _offsetRead = 0
    _dataLength = 0
    _data = None

    _headers = {}
    _media_type = ''

    def __init__(self, prefix, root):
        self._root = root
        self._prefix = prefix
        Console().debug("LR ROOT: '{}'".format(self._root))

    def LoadResource(self, path):
        if path[0] == '/':
            path = path[1:]
        location = os.path.join( self._root, path )
        Console().debug("LR load: '{}'".format(location))

        if not os.path.exists( location ):
            return None

        file = open( location, 'rb' )
        content = file.read()
        file.close()

        return content

    def ProcessRequest(self, request, callback):
        Console().debug("ProcessRequest(): ({})'{}'".format(request.GetMethod(),request.GetUrl()))
        # 1. Start the request using WebRequest
        # 2. Return True to handle the request
        # 3. Once response headers are ready call
        #    callback.Continue()

        # Need to set AllowCacheCredentials and AllowCookies for
        # the cookies to work during POST requests (Issue 127).
        # To skip cache set the SkipCache request flag.
        request.SetFlags(cefpython.Request.Flags["AllowCachedCredentials"]\
                | cefpython.Request.Flags["AllowCookies"])

        url = request.GetUrl()
        start = url.index(self._prefix) + len(self._prefix)

        res_name = url[start:]

        Console().debug("resource name '{}'".format(res_name))

        self._data = self.LoadResource(res_name)

        if not self._data:
            self._headers = {
                'status': 404,
                'content-length': 0,
            }
            callback.Continue()
            return True


        self._media_type = content_type = 'application/octet-stream'
        mime_type, encoding = mimetypes.guess_type(res_name)
        if mime_type:
            content_type = mime_type
            self._media_type = mime_type

        if content_type.startswith('text'):
            content_type += '; charset=utf-8'

        # if res_name.endswith('html'):
        #     content_type = 'text/html; charset=utf-8'
        #     self._media_type = 'text/html'
        # elif res_name.endswith('css'):
        #     content_type = 'text/css; charset=utf-8'
        #     self._media_type = 'text/css'
        self._dataLength = len(self._data)

        # https://en.wikipedia.org/wiki/List_of_HTTP_header_fields#Standard_response_fields
        self._headers = {
            'status': 200,
            'content-type': content_type,
            'content-length': self._dataLength,
        }
        Console().debug("self._headers = {}".format(self._headers))

        # We have constructed response heaaders so we can call callback right now
        callback.Continue()

        return True

    def GetResponseHeaders(self, response, response_length_out, redirect_url_out):
        Console().debug("GetResponseHeaders()")
        # 1. If the response length is not known set
        #    response_length_out[0] to -1 and ReadResponse()
        #    will be called until it returns False.
        # 2. If the response length is known set
        #    response_length_out[0] to a positive value
        #    and ReadResponse() will be called until it
        #    returns False or the specified number of bytes
        #    have been read.
        # 3. Use the |response| object to set the mime type,
        #    http status code and other optional header values.
        # 4. To redirect the request to a new URL set
        #    redirectUrlOut[0] to the new url.

        response.SetStatus(self._headers['status'])
        response.SetStatusText('OK')
        response.SetMimeType(self._media_type)

        Console().debug("headers: ")
        Console().debug(self._headers)
        response_length_out[0] = self._dataLength

        if not response_length_out[0]:
            # Wha!??
            pass

    def ReadResponse(self, data_out, bytes_to_read, bytes_read_out, callback):
        Console().debug("ReadResponse()")

        # 1. If data is available immediately copy up to
        #    bytes_to_read bytes into data_out[0], set
        #    bytes_read_out[0] to the number of bytes copied,
        #    and return true.
        # 2. To read the data at a later time set
        #    bytes_read_out[0] to 0, return true and call
        #    callback.Continue() when the data is available.
        # 3. To indicate response completion return false.
        if self._offsetRead < self._dataLength:
            dataChunk = self._data[\
                    self._offsetRead:(self._offsetRead + bytes_to_read)]
            self._offsetRead += len(dataChunk)
            data_out[0] = dataChunk
            bytes_read_out[0] = len(dataChunk)
            return True

        self._clientHandler._ReleaseStrongReference(self)
        Console().debug("    no more data")
        return False

    def CanGetCookie(self, cookie):
        # Return true if the specified cookie can be sent
        # with the request or false otherwise. If false
        # is returned for any cookie then no cookies will
        # be sent with the request.
        return True

    def CanSetCookie(self, cookie):
        # Return true if the specified cookie returned
        # with the response can be set or false otherwise.
        return True

    def Cancel(self):
        # Request processing has been canceled.
        pass
