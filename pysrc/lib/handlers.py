## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from utils import Console


class DisplayHandler(object):
    def OnConsoleMessage(self, browser, level, message, source, line):
        """Called to display a console message."""
        if not 'data:' in source:
            Console().debug( source )
        Console().debug( "L: {}, level: {}".format( line, level ) )
        # level is one of:
        # cef.LOGSEVERITY_VERBOSE
        # cef.LOGSEVERITY_INFO
        # cef.LOGSEVERITY_WARNING
        # cef.LOGSEVERITY_ERROR # default
        # cef.LOGSEVERITY_DISABLE
        # This will intercept js errors, see comments in OnAfterCreated
        if "error" in message.lower() or "uncaught" in message.lower():
            Console().error("[OnConsoleMessage -> error] '{:s}'".format(message))
        else:
            Console().info("[OnConsoleMessage -> message] '{:s}'".format(message))


class LoadHandler(object):
    def OnLoadingStateChange(self, browser, is_loading, **_):
        """Called when the loading state has changed."""
        if not is_loading:
            # Loading is complete. DOM is ready.
            Console().debug('Loading is complete. DOM is ready.')

    def OnLoadError(self, browser, frame, error_code, error_text_out, failed_url):
        # error_code -> https://src.chromium.org/viewvc/chrome/trunk/src/net/base/net_error_list.h?view=markup
        Console().error("Failed URL: '{:s}'".format(failed_url))
        Console().error("Error Code: '{:d}'".format(error_code))

    def OnLoadError(self, browser, frame):
        # https://github.com/cztomczak/cefpython/issues/284
        browser.SetFocus(True)

class GlobalHandler(object):
    def OnAfterCreated(self, browser, **_):
        """Called after a new browser is created."""
        # DOM is not yet loaded. Using js_print at this moment will
        # throw an error: "Uncaught ReferenceError: js_print is not defined".
        # We make this error on purpose. This error will be intercepted
        # in DisplayHandler.OnConsoleMessage.
        Console().debug('New browser has been created')
