## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

from cefpython3 import cefpython as cef

import os, sys
import base64
import platform

from singleton import Singleton


def get_main_script_path():
    main_file = os.path.realpath( sys.argv[0] ) if sys.argv[0] else None
    return main_file


class Console(Singleton):

    NONE = 0
    INFO = 1
    WARNING = 2
    DEBUG = 3

    level = INFO

    def __init__(self):
        pfx = get_main_script_path()
        if not pfx:
            pfx = '__app__'
        self.prefix = os.path.basename( pfx )

    def _log(self, tag, msg):
        print("[{:s} - {:s}] {:s}".format(self.prefix, tag, msg))

    def error(self, msg):
        self._log( 'ERROR', msg )

    def info(self, msg):
        if Console.level >= Console.INFO:
            self._log( 'INFO', msg )

    def warning(self, msg):
        if Console.level >= Console.WARNING:
            self._log( 'WARNING', msg )

    def debug(self, msg):
        if Console.level >= Console.DEBUG:
            self._log( 'DEBUG', msg )



def html_to_data_uri(html):
    html = html.encode("utf-8", "replace")
    b64 = base64.b64encode(html).decode("utf-8", "replace")
    ret = "data:text/html;base64,{data}".format(data=b64)
    return ret


def check_versions():
    ver = cef.GetVersion()
    Console().info("CEF Python {ver}".format(ver=ver["version"]))
    Console().info("Chromium {ver}".format(ver=ver["chrome_version"]))
    Console().info("CEF {ver}".format(ver=ver["cef_version"]))
    Console().info("Python {ver} {arch}".format(
           ver=platform.python_version(),
           arch=platform.architecture()[0]))
    assert cef.__version__ >= "57.0", "CEF Python v57.0+ required to run this"


def get_application_path(file=None):
    import re, os, platform
    # On Windows after downloading file and calling Browser.GoForward(),
    # current working directory is set to %UserProfile%.
    # Calling os.path.dirname(os.path.realpath(__file__))
    # returns for eg. "C:\Users\user\Downloads". A solution
    # is to cache path on first call.
    if not hasattr(get_application_path, "dir"):
        if hasattr(sys, "frozen"):
            dir = os.path.dirname( sys.executable )
        elif get_main_script_path():
            dir = os.path.dirname( os.path.realpath( get_main_script_path() ) )
        else:
            dir = os.getcwd()
        get_application_path.dir = dir
    # If file is None return current directory without trailing slash.
    if file is None:
        file = ""
    # Only when relative path.
    if not file.startswith("/") and not file.startswith("\\") and (
            not re.search(r"^[\w-]+:", file)):
        path = get_application_path.dir + os.sep + file
        if platform.system() == "Windows":
            path = re.sub(r"[/\\]+", re.escape(os.sep), path)
        path = re.sub(r"[/\\]+$", "", path)
        return path
    return str(file)
