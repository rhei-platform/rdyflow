## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later

# Tested with CEF Python v57.0+

from cefpython3 import cefpython as cef
import sys

# set window size
import platform
import ctypes

# https://github.com/cztomczak/cefpython/issues/432
import tempfile

from lib.client_handler import ClientHandler
from lib.utils import check_versions, get_application_path, Console
from lib.handlers import LoadHandler, DisplayHandler, GlobalHandler



def set_client_handlers( browser, uri_prefix, resources_folder ):

    client_handlers = [
        LoadHandler(),
        DisplayHandler(),
        ClientHandler( uri_prefix, get_application_path( resources_folder ) ),
    ]

    for handler in client_handlers:
        browser.SetClientHandler(handler)


def set_defaults( settings ):

    if not 'resources_uri_prefix' in settings:
        settings['resources_uri_prefix'] = 'myapp'

    if not 'resources_folder' in settings:
        settings['resources_folder'] = 'html'

    if not 'window_title' in settings:
        settings['window_title'] = 'My Application'

    if not 'cache_temp_folder' in settings:
        settings['cache_temp_folder'] = 'cefpy'

    if not 'window_width' in settings:
        settings['window_width'] = 900

    if not 'window_height' in settings:
        settings['window_height'] = 640

    return settings


def cef_app( app_init_callback = None, index_page = 'index.html', app_settings = {} ):

    set_defaults( app_settings )

    check_versions()

    cache_path = tempfile.gettempdir() + '/' + app_settings['cache_temp_folder']
    Console().debug("CEF cache path '{path}'".format(path=cache_path))

    # To change user agent use either "product_version" or "user_agent" options
    settings = {
        # "product_version": "MyProduct/10.00",
        # "user_agent": "MyAgent/20.00 MyProduct/10.00",
        # https://github.com/cztomczak/cefpython/issues/432
        "cache_path": cache_path,
        # "log_file": "debug.log",
        # "log_severity": cef.LOGSEVERITY_VERBOSE,
    }

    sys.excepthook = cef.ExceptHook  # To shutdown all CEF processes on error

    cef.Initialize(settings=settings)

    # A global handler is a special handler for callbacks that
    # must be set before Browser is created using
    # SetGlobalClientCallback() method.
    global_handler = GlobalHandler()
    cef.SetGlobalClientCallback("OnAfterCreated", global_handler.OnAfterCreated)

    # set inital window size hack
    window_info = cef.WindowInfo()
    parent_handle = 0
    window_width = app_settings['window_width']
    window_height = app_settings['window_height']
    # This call has effect only on Mac and Linux.
    # All rect coordinates are applied including X and Y parameters.
    window_info.SetAsChild( parent_handle, [0, 0, window_width, window_height] )

    browser = cef.CreateBrowserSync(url='', window_info=window_info, window_title=app_settings['window_title'])

    if platform.system() == "Windows":
        window_handle = browser.GetOuterWindowHandle()
        insert_after_handle = 0
        # X and Y parameters are ignored by setting the SWP_NOMOVE flag
        SWP_NOMOVE = 0x0002
        # noinspection PyUnresolvedReferences
        ctypes.windll.user32.SetWindowPos(  window_handle, insert_after_handle,
                                            0, 0, window_width, window_height, SWP_NOMOVE )

    set_client_handlers( browser, app_settings['resources_uri_prefix'], app_settings['resources_folder'] )

    if app_init_callback:
        app_init_callback(browser)

    browser.LoadUrl("http://{}/{}".format( app_settings['resources_uri_prefix'], index_page ))

    cef.MessageLoop()
    del browser
    cef.Shutdown()
