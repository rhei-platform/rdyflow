## Copyright 2019 Ravendyne Inc.
## SPDX-License-Identifier: GPL-3.0-or-later


from cefpython3 import cefpython

import re
# import time
# import mimetypes
from resources import LocalResourceHandler
from utils import Console


# https://github.com/cztomczak/cefpython/blob/master/api/RequestHandler.md
class ClientHandler:

    def __init__(self, prefix, localResourcePath):
        self._localResourcePath = localResourcePath
        self._prefix = prefix
        Console().debug("ClientHandler(): localResourcePath = %s" % localResourcePath)
        Console().debug("ClientHandler(): prefix = %s" % prefix)

    # RequestHandler.GetResourceHandler()
    def GetResourceHandler(self, browser, frame, request):
        # Called on the IO thread before a resource is loaded.
        # To allow the resource to load normally return None.
        Console().debug("GetResourceHandler(): url = %s" % request.GetUrl())

        if self._prefix in request.GetUrl():
            resHandler = LocalResourceHandler( self._prefix, self._localResourcePath )
            resHandler._clientHandler = self
            self._AddStrongReference(resHandler)
            return resHandler

        return None

    # A strong reference to LocalResourceHandler must be kept
    # during the request. Some helper functions for that.
    # 1. Add reference in GetResourceHandler()
    # 2. Release reference in LocalResourceHandler.ReadResponse()
    #    after request is completed.

    _resourceHandlers = {}
    _resourceHandlerMaxId = 0

    def _AddStrongReference(self, resHandler):
        self._resourceHandlerMaxId += 1
        resHandler._resourceHandlerId = self._resourceHandlerMaxId
        self._resourceHandlers[resHandler._resourceHandlerId] = resHandler

    def _ReleaseStrongReference(self, resHandler):
        if resHandler._resourceHandlerId in self._resourceHandlers:
            del self._resourceHandlers[resHandler._resourceHandlerId]
        else:
            Console().error("_ReleaseStrongReference() FAILED: resource handler " \
                    "not found, id = %s" % (resHandler._resourceHandlerId))

