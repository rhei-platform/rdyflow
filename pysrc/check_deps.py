deps_missing = False

try:
    import cefpython3
except ImportError:
    cefpython3 = None
    deps_missing = True
    print(  "Error: 'cefpython3' package missing. "
            "To install type: pip install --upgrade cefpython3")

# try:
#     import serial
# except ImportError:
#     serial = None
#     deps_missing = True
#     print(  "Error: 'pyserial' package missing. "
#             "To install type: pip install --upgrade pyserial")

if deps_missing:
    exit(1)

